<?php
	require_once("inc/config.php");
	require("items/items.php");
	require("inc/functions.php");

if ( isset($_POST["date_from"]) && !empty($_POST["date_from"]) && isset($_POST["date_to"]) && !empty($_POST["date_to"]) ) {

	header('Content-Encoding: UTF-8');
	header('Content-type: text/csv; charset=UTF-8');
	header("Content-Disposition: attachment; filename=MQI_export.csv");
	header("Pragma: no-cache");
	header("Expires: 0");

	$date_from = $_POST["date_from"];
	$date_to = $_POST["date_to"];

	$output = fopen('php://output', 'w');

	$user_list = get_role_users( 'mqi' );

// Export findings details

	$title = array ("Date", 
					"Month",
					"MWC", 
					"Sale Office",
					"Technician",
					"Project Name",
					"Equipment No.",
					"Lift No",
					"Finding Items",
					"Finding Details",
					"Finding Remark",
					"Status",
					"Days");

	fputcsv($output, $title);

	foreach ($user_list as $user_name) {

		$table_findings = $user_name."_findings";
		$table_siteInfos= $user_name."_siteInfos";

		require("inc/database.php");

		$query = "
			
			SELECT 	DATE_FORMAT(S.date_check, '%d-%m-%Y'),
					MONTH(S.date_check), 
				   	S.MWC, 
				   	S.sales_office, 
				   	S.EI_tech_1, 
				   	S.site_name, 
				   	S.equip_no, 
				   	S.lift,
				   	F.index,
				   	F.descriptions,
				   	F.remarks,
				   	F.status,
				   	F.date_close
			FROM   	$table_siteInfos AS S,
				   	$table_findings  AS F
			WHERE  	S.count = F.count
			AND 	S.date_check >= '$date_from'
			AND 	S.date_check <= '$date_to'
			AND    	F.descriptions != 'NEW SESSION'
		";

		try {
			$results = $db->query($query);
		} catch (Exception $e) {
			echo "Cannot connect to database: EXPORT.PHP";
			print_r( $db->errorinfo());
			exit;
		}	

		$results = $results->fetchAll(PDO::FETCH_ASSOC);

		foreach ($results as $key_site => $site) {

			fputcsv($output, $site);
			
		}
	}

// Export site results

	$title_result = array (	"MWC", 
							"Sale Office",
							"Project Name", 
							"Equipment No.",
							"Unit",
							"Inspection Date",
							"Month",
							"No. of Critical Findings",
							"Fitter Score",
							"Unit Score",
							"FINAL RESULT",
							"Re-INSPECT"
							);

	fputcsv($output, $title_result);

	foreach ($user_list as $user_name) {

		$table_scoring = $user_name."_scoring";
		$table_siteInfos= $user_name."_siteInfos";

		require("inc/database.php");

		$query = "			
			SELECT 	S.MWC,
					S.sales_office,
					S.site_name,
					S.equip_no,
					S.lift,
					DATE_FORMAT(S.date_check, '%d-%m-%Y'),
					MONTH(S.date_check),
				   	SC.MUS,
				   	SC.MFS,
				   	SC.total_score,
				   	SC.count,
				   	S.reinspection

			FROM   	$table_siteInfos AS S,
				   	$table_scoring  AS SC
			WHERE  	S.count = SC.count
			AND 	S.date_check >= '$date_from'
			AND 	S.date_check <= '$date_to'
		";

		try {
			$results = $db->query($query);
		} catch (Exception $e) {
			echo "Cannot connect to database: EXPORT.PHP";
			print_r( $db->errorinfo());
			exit;
		}	

		$results = $results->fetchAll(PDO::FETCH_ASSOC);

		$i = 0;
		foreach ($results as $key_site => $site) {

			$no_critical = number_of_critical_items($user_name, $site["count"], $critical_item);
			$MUS_score = $site["MUS"]/$site["total_score"];
			$MFS_score = $site["MFS"]/$site["total_score"];

			// $MUS_score_form = round( (100*($site["MUS"]/$site["total_score"])) , 2 )."%";
			// $MFS_score_form = round( (100*($site["MFS"]/$site["total_score"])) , 2 )."%";

			if ( $no_critical > 0 || ($MUS_score*100) < 80 || ($MFS_score*100) < 80 ) {
				$final_result = "FAIL";
			} else {
				$final_result = "PASS";
			}
			if ( $site["reinspection"] == 1 ) {
				$reinspection = "NO";
			} elseif ( $site["reinspection"] == 2 )  {
				$reinspection = "YES";
			}

			$site_results_array[$i][0] = $site["MWC"];
			$site_results_array[$i][1] = $site["sales_office"];
			$site_results_array[$i][2] = $site["site_name"];
			$site_results_array[$i][3] = $site["equip_no"];
			$site_results_array[$i][4] = $site["lift"];
			$site_results_array[$i][5] = $site["DATE_FORMAT(S.date_check, '%d-%m-%Y')"];
			$site_results_array[$i][6] = $site["MONTH(S.date_check)"];
			$site_results_array[$i][7] = $no_critical;
			$site_results_array[$i][8] = $MFS_score;
			$site_results_array[$i][9] = $MUS_score;
			$site_results_array[$i][10] = $final_result;
			$site_results_array[$i][11] = $reinspection;

			fputcsv($output, $site_results_array[$i]);

			$i++;
			
		}
	}
}

?>

<!DOCTYPE html>
<html>
<head></head>
<body>
	<form method="POST" action="export.php">
		<p>Date from: <input type="date" name="date_from" required></p>
		<p>Date to: <input type="date" name="date_to" required></p>
		<input type="submit" value="download">
	</form>
</body>
</html>