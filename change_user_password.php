<?php
$message_change = "";

if ( isset($_POST["old_change_pass"]) && isset($_POST["new_change_pass"]) && isset($_POST["new_change_pass_confirm"]) ) {
	$old_change_pass = $_POST["old_change_pass"];
	$old_change_pass_hash5 = md5($old_change_pass);
	$new_change_pass = $_POST["new_change_pass"];
	$new_change_pass_hash5 = md5($new_change_pass);
	$new_change_pass_confirm = $_POST["new_change_pass_confirm"];
	if ( !empty($old_change_pass) && !empty($new_change_pass) && !empty($new_change_pass_confirm) ) {
		$current_pass_hash5 = get_the_password( $_SESSION["user_name"] );
		if ( $current_pass_hash5 != $old_change_pass_hash5 ) {
			$message_change = "Bạn đã nhập sai password hiện tại!";
			$flag = 1;
		} else {
			if ( $new_change_pass != $new_change_pass_confirm ) {
				$message_change = "2 lần nhập password mới của bạn không khớp nhau!";
				$flag = 1;
			} else {
				$username = $_SESSION["user_name"];
				require("inc/database.php");
				try {
					$results = $db->query("
						UPDATE 	users 
						SET 	password = '$new_change_pass_hash5'
						WHERE 	username = '$username'
						");
				} catch (Exception $e) {
					echo "Không thể kết nối database ở mục: change_user_password.php";
					print_r( $db->errorinfo() );
					exit;
				}
				$message_change = "Chúc mừng bạn đã đổi thành công password";
				$flag = 2;
			}
		}

	}
}

?>

<form method="POST" action="<?php echo $current_file; ?>">
	<table class="change-pass-table">
		<tr class="change-pass-title">
			<td colspan='2'>Đổi password</td>
		</tr>
		<tr>
			<td>Password hiện tại</td>
			<td><input type="password" name="old_change_pass" class="change-pass-fill" required /></td>
		</tr>
		<tr>
			<td>Password mới</td>
			<td><input type="password" name="new_change_pass" class="change-pass-fill" required /></td>
		</tr>
		<tr>
			<td>Nhập lại password mới</td>
			<td><input type="password" name="new_change_pass_confirm" class="change-pass-fill" required /></td>
		</tr>
		<tr>
			<td colspan='2'><input type="submit" name="submit" value="Thay đổi" class="button"/></td>
		</tr>
		<?php if ( !empty($message_change) ) { ?>
		<tr <?php if ( $flag == 1 ) {
				echo 'class="new-user-alert1"';
			} elseif ( $flag == 2 ) {
				echo 'class="new-user-alert2"';
			}
		?> >
			<td colspan='2'><?php echo $message_change; ?></td>
		</tr>
		<?php }	?>
	</table>
</form>