///////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////                     DATEPICKER JQUERY                         //////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
$( "#form_date" ).datepicker({
	dateFormat: "dd-mm-yy",
	numberOfMonths: 1,
	changeMonth: true,
	monthNamesShort: [ "T.1", "T.2", "T.3", "T.4", "T.5", "T.6", "T.7", "T.8", "T.9", "T.10", "T.11", "T.12"]
});
///////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////                     SCORES IN REVIEW                          //////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
function unitScoreCal($rank, $weight) {// The function that calculates the site score
	var $score = 0;
	if ( $rank === -3 || $rank === -2 || $rank === -1 ) {
		$score = $rank * $weight;
	} else if ( $rank === 1 || $rank === 1.5 || $rank === 0 ) {
		$score = $rank;
	} else {
		console.log("Errors in the function of unitScoreCal in form.js");
	}
	return $score;
}
function fitterScoreCal($rank, $weight, $respo) {// The function that calculates the fitter score
	var $score = 0;
	if ( $respo == 1 ) {
		$score = 1;
	} else {
		if ( $rank === -3 || $rank === -2 || $rank === -1 ) {
			$score = $rank * $weight;
		} else if ( $rank === 1 || $rank === 1.5 || $rank === 0 ) {
			$score = $rank;
		} else {
			console.log("Errors in the function of fitterScoreCal in form.js");
		}
	}
	return $score;
}
function totalScoreCal($rank) {// The function that calculates the maximum score
	var $score = 0;
	if ($rank === 0) {
		$score = 0;
	} else {
		$score = 1;
	}
	return $score;
}
// Insert the NCs to the review
function insertNCsToReview($index, $item, $rank, $weight, $describe, $remark, $status, $respo) {
	var $divNC = $('<div class="review-NCs"></div>');
	var $indexSpan = $('<span>' + $index + '</span>');
	var $itemPara = $('<p class="review-NCs-item">' + $item + '</p>');
	var $rankLabel = $('<label>Đánh giá</label>');
	if ( $rank === 0 ) {
		$rank = 'không đánh giá';
	}
	var $rankPara = $('<p>' + $rank + '</p>');
	var $weightLabel = $('<label>Trọng số</label>');
	var $weightPara = $('<p>' + $weight + '</p>');
	var $desLabel = $('<label>Mô tả lỗi</label>');
	var $desPara = $('<p>' + $describe + '</p>');
	var $remarkLabel = $('<label>Ghi chú</label>');
	var $remarkPara = $('<p>' + $remark + '</p>');
	var $statusLabel = $('<label>Tình trạng</label>');
	var $statusPara = $('<p>' + $status + '</p>');
	var $respoLabel = $('<label>Trách nhiệm</label>');
	if ( $respo === 2 ) {
		$respo = 'có';
	} else if ( $respo === 1 ) {
		$respo = 'không';
	}
	var $respoPara = $('<p>' + $respo + '</p>');
	// Append the divs to the div of NCs
	$divNC.append($indexSpan);
	$divNC.append($itemPara);
	$divNC.append($rankLabel);
	$divNC.append($rankPara);
	$divNC.append($weightLabel);
	$divNC.append($weightPara);
	$divNC.append($desLabel);
	$divNC.append($desPara);
	$divNC.append($remarkLabel);
	$divNC.append($remarkPara);
	$divNC.append($statusLabel);
	$divNC.append($statusPara);
	$divNC.append($respoLabel);
	$divNC.append($respoPara);
	// Append the divNC to the review
	$('.NCs-container').append($divNC);
}
function siteInfoToReview( $siteName, $formDate, $lift, $mrMrl, $sup, $technician, $reinspection, $equipNo, $MWC, $saleOffice) {
	$('.review-site-info p').text( $siteName );
	$('.review-date-check p').text( $formDate );
	$('.review-lift-name p').text( $lift );
	$('.review-MR-type p').text( $mrMrl );
	$('.review-sup-name p').text( $sup );
	$('.review-EI-technician p').text( $technician );
	$('.review-checked p').text( $reinspection );
	$('.review-unit-number p').text( $equipNo );
	$('.review-MWC p').text( $MWC );
	$('.review-sale-office p').text( $saleOffice );
}
// Hide elements
function hideElements( element ) {
	$(element).hide();
}
// Remove elements
function removeElements( element ) {
	$(element).remove();
}
// To add the arrows to navigate the NCs
function addArrows() {
	var $leftArrow = $('<div class="left-arrow"></div>');
	var $rightArrow = $('<div class="right-arrow"></div>');
	$('.NCs-container').append($leftArrow);
	$('.NCs-container').append($rightArrow);
}
// Append the submit button to the overlay
$('.form-in-review').append( $('.button-submit') );
// Hide the overlay
$('#overlay').hide();
// Review flag - to identify clicking the review button for the first time
var $reviewFlag = 0;
// Declare global curruent NC
var $currentNC;
// Review the form submission
$('.button-review').click(function(){
	// Step the review flag
	$reviewFlag++;
	var $unitScore = 0;
	var $fitterScore = 0;
	var $totalScore = 0;
	var $rank = 0;
	var $weight = 0;
	var $respo = 0;
	var $numberOfCheckpoints = $(".tab-group").find("legend").length;
	// Flag to show the arrows
	var $arrowFlag = 0;
	for ( var $i = 1; $i <= $numberOfCheckpoints; $i++ ) {
		$rank = $(".rank-form-" + $i).val();
		$rank = parseInt($rank, 10);// Convert to number
		$weight = $(".weight-" + $i).text();
		$weight = parseInt($weight, 10);// Convert to number
		$respo = $(".res-form-" + $i).val();
		$respo = parseInt($respo, 10);// Convert to number
		// Calculate the score of the unit
		$unitScore = $unitScore + unitScoreCal($rank, $weight);
		// Calculate the score of the fitter
		$fitterScore = $fitterScore + fitterScoreCal($rank, $weight, $respo);
		// Calculate the maximum score
		$totalScore = $totalScore + totalScoreCal($rank);
		// Show the NCs on the review
		if ($rank !== 1) {
			$index = $(".index-form-" + $i).text();
			$item = $(".item-form-" + $i).text();
			$describe = $(".des-form-" + $i).val();
			$remark = $(".remark-form-" + $i + " option:selected").text();
			$status = $(".status-form-" + $i + " option:selected").text();
			insertNCsToReview($index, $item, $rank, $weight, $describe, $remark, $status, $respo);
			// Turn on the flag
			$arrowFlag = 1;
		}
	}
	// Define the current NC to show is the first NC
	$currentNC = $('.review-NCs').first();
	if ( $arrowFlag === 1 ) {
		// Hide all NCs
		hideElements('.review-NCs');
		// Show the first NC
		$currentNC.show();
	}
	// Show the arrows if the flag = 1 and no arrow classes
	if ( $arrowFlag === 1 && !$('.left-arrow').length ) {
		addArrows();
	}
	// Fitter score and site score in percentage
	var $fitterScorePer = Math.round($fitterScore/$totalScore*100);
	var $unitScorePer = Math.round($unitScore/$totalScore*100);
	// Insert the site infos to the review
	siteInfoToReview(
		$('#site_name').val(),
		$('#form_date').val(),
		$('#lift').val(),
		$('#MR_MRL option:selected').text(),
		$('#sup option:selected').text(),
		$('#EI_tech_1').val(),
		$('#reinspection option:selected').text(),
		$('#equip_no').val(),
		$('#MWC option:selected').text(),
		$('#sales_office option:selected').text()
	);
	// Insert scores to the review
	$('.review-fitter-score .score').text( $fitterScore );
	$('.review-site-score .score').text( $unitScore );
	$('.review-total-score .score').text( $totalScore );
	$('.review-fitter-score .scorePer').text( $fitterScorePer + " %" );
	$('.review-site-score .scorePer').text( $unitScorePer + " %" );
	if ( $fitterScorePer > 80 && $unitScorePer > 80 ) {
		$('.review-result .score').text( 'Đạt' );
		$('.review-result .score').css('background', '#67ED61');
	} else {
		$('.review-result .score').text( 'Rớt' );
		$('.review-result .score').css('background', '#EDAF61');
	}
	// Show the overlay
	$('#overlay').show();
	//Those functions below need to be read 1 time, cuz problem occurs if being read many times
	if ( $reviewFlag === 1 ) {
		// Navigate the NCs using arrows
		// Click on the left arrow
		$('.right-arrow').click(function() {
			var $rightNC = $currentNC.next('.review-NCs');
			if ( $rightNC.length ) {
				$currentNC.hide();
				$rightNC.show();
				$currentNC = $rightNC;
			}
		});
		// Click on the right arrow
		$('.left-arrow').click(function() {
			var $leftNC = $currentNC.prev('.review-NCs');
			if ( $leftNC.length ) {
				$currentNC.hide();
				$leftNC.show();
				$currentNC = $leftNC;
			}
		});
	}
});

// Close the review
$('#overlay img').click(function() {
	$('#overlay').hide(); // to hide the overlay
	removeElements('.review-NCs'); // To remove all NCs
	$currentNC = null; // Clear the current NC
});