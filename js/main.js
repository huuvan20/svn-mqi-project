//////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// MULTISELECT JQUERY PLUGIN ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
$('#sales_office_search').multiselect({
	includeSelectAllOption: true,
	maxHeight: 200,
	//buttonWidth: '450px'
	nonSelectedText: 'Sales office'
});

$('#MWC_search').multiselect({
	includeSelectAllOption: true,
	maxHeight: 200,
	buttonWidth: '120px',
	nonSelectedText: 'MWC',
	enableCaseInsensitiveFiltering: true
});

$('#status_search').multiselect({
	includeSelectAllOption: true,
	maxHeight: 200,
	buttonWidth: '120px',
	nonSelectedText: 'Status'
});

$('#remark_search').multiselect({
	includeSelectAllOption: true,
	maxHeight: 200,
	buttonWidth: '120px',
	nonSelectedText: 'Remark'
});


//////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////// UI JQUERY PLUGIN (DATEPICKER) IN MONITORS.PHP and MQI_results.php ////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

$( "#from" ).datepicker({
	dateFormat: "dd-mm-yy",
	defaultDate: "-2m",
	changeMonth: true,
	numberOfMonths: 3,
	monthNamesShort: [ "T.1", "T.2", "T.3", "T.4", "T.5", "T.6", "T.7", "T.8", "T.9", "T.10", "T.11", "T.12"],
	dayNamesShort: [ "CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy" ],
	onClose: function( selectedDate ) {
		$( "#to" ).datepicker( "option", "minDate", selectedDate );
  }
});
$( "#to" ).datepicker({
	dateFormat: "dd-mm-yy",
	defaultDate: "-2m",
  	changeMonth: true,
  	numberOfMonths: 3,
  	monthNamesShort: [ "T.1", "T.2", "T.3", "T.4", "T.5", "T.6", "T.7", "T.8", "T.9", "T.10", "T.11", "T.12"],
  	dayNamesShort: [ "CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy" ],
  	onClose: function( selectedDate ) {
		$( "#from" ).datepicker( "option", "maxDate", selectedDate );
  }
});











