<?php 
	
	if ( isset($_POST["reset_user"]) && isset($_POST["admin_password"]) && isset($_POST["reset_password"]) && isset($_POST["reset_password_again"]) ) {
		$reset_user = $_POST["reset_user"];
		$admin_password = $_POST["admin_password"];
		$reset_password = $_POST["reset_password"];
		$reset_password_again = $_POST["reset_password_again"];
		if ( !empty($reset_user) && !empty($admin_password) && !empty($reset_password) && !empty($reset_password_again) ) {
			$pass_hash = get_pass_from_user( $_SESSION["user_name"] );// Get the current admin's password from database
			$admin_password_hash = md5($admin_password);
			if ( $pass_hash != $admin_password_hash) { // Check if the admin enter his right password
				$message_reset = "Bạn nhập sai password của bạn!";
				$flag = 1;
			} else {
				if ( $reset_password != $reset_password_again ) { // Check if the reset password and the confirmed reset password are the same
					$message_reset = "2 lần nhập password reset không đúng!";
					$flag = 1;
				} else {
					$reset_password_hash = md5($reset_password); // Hash the reset password to write to database
					require("inc/database.php");
					try { // Write new reset password to the database
						$results = $db->query("
							UPDATE users
							SET password = '$reset_password_hash'
							WHERE username = '$reset_user'
							");
					} catch (Exception $e) {
						echo "Không thể ghi đè password trong reset user password";
						print_r( $db->errorinfo() );
						exit;
					}
					$message_reset = "Chúc mừng, bạn đã reset thành công password của user: ".$reset_user;
					$flag = 2;
				}
			}
		} 
	}

?>

<form method="POST" action="<?php echo $current_file; ?>">
	<table class="new-user-table">

		<tr class="new-user-title">
			<td colspan='2'>Reset user password</td>
		</tr>

		<tr>
			<td>Choose username</td>
			<td>
				<select name="reset_user" class="new-user-fill" required>
					<?php 
						foreach ($user_role_list as $user_role) {
							if ( $user_role != "admin" ) {
								$users_per_role = get_role_users( $user_role );
								foreach ($users_per_role as $username) {
					?>
					<option value="<?php echo $username; ?>">
						<?php echo $username; ?>
					</option>
					<?php		}
							}
						}
					?>
				</select>
			</td>
		</tr>

		<tr>
			<td>Your password</td>
			<td><input type="password" name="admin_password" class="new-user-fill" maxlength="50" required /></td>
		</tr>

		<tr>
			<td>Reset password</td>
			<td><input type="password" name="reset_password" class="new-user-fill" maxlength="50" required /></td>
		</tr>

		<tr>
			<td>Confirm reset password</td>
			<td><input type="password" name="reset_password_again" class="new-user-fill" maxlength="50" required /></td>
		</tr>

		<tr>
			<td colspan='2'>
				<input type="submit" name="submit" value="Reset" class="button"/>
			</td>
		</tr>

		<?php if ( isset($message_reset) && !empty($message_reset) ) { ?>
		<tr 
		<?php if ( $flag == 1 ) {
				echo 'class="new-user-alert1"';
			} elseif ( $flag == 2 ) {
				echo 'class="new-user-alert2"';
			}
		?> >
			<td colspan='2'><?php echo $message_reset;?></td>
		</tr>
		<?php } ?>

	</table>
</form>