<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <title>MQI | Schindler VN</title>
	    <meta name="viewport" content="width=device-width">
	    <link rel="stylesheet" href="css/normalize.css">
	    <link href='http://fonts.googleapis.com/css?family=Changa+One|Open+Sans:400,400italic,700,700italic,800' rel='stylesheet' type='text/css'>
	    <link rel="stylesheet" href="css/main.css">
	</head>
	<body>
		<header>
			<a id="logo">
	    	<h1>MQI</h1>
	    	<h2>Schindler VN</h2>
		  </a>
		</header>
		<div id="wrapper">

		<section>
			<table class="dashboard-table">
				<tr class="tableheader">
					<th colspan="2">
						Cám ơn vì sự giúp đỡ tuyệt vời của bạn với MQI!
					</th>
				</tr>
				<tr class="tablerow">
					<td class="dashboard-td1">
						Vâng, <a href="back_to_form.php" tite="logout">tôi muốn tiếp tục gửi báo cáo MQI</a>
					</td>

					<td class="dashboard-td2">			
						Không, <a href="logout.php" tite="logout">tôi muốn thoát ra</a>
					</td>
				</tr>
			</table>
		</section>
<?php 

require_once("inc/config.php");
include("footer.php");

?>