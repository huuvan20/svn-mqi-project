<?php 

require_once("inc/config.php");
require("inc/functions.php");
require("items/items.php");
include("header.php");

include("inc/session_timeout.php");

?>

<section class="div-index">
<?php
	if ( loggedin() ) {
		// Get name and say hello
		$fullname = get_user_fullname($_SESSION["user_id"]);
		echo "<p>Chào mừng ". $fullname ." đến với Schindler VN MQI</p>";
		
		// check the user's role for specific privileges
		$admins_list = get_role_users( 'admin' );
		if ( in_array($_SESSION["user_name"], $admins_list) ) {
			include 'create_new_users.php';
			include 'reset_user_pass.php';
		}
		include 'change_user_password.php';

	} else {
		// show the required login before accessing anythings
		include 'user_login_form.php';

	}
?>
</section>

<?php 

include("footer.php");

?>

