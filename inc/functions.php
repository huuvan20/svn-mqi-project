<?php

ob_start();
session_start();

$current_file = $_SERVER['SCRIPT_NAME'];  // url of the current page
//$http_referer = $_SERVER['HTTP_REFERER']; // url of the last page you're in 

// Loggedin function
function loggedin () {

	if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
		return true;
	} else {
		return false;
	}
}

// Get the login time currently corresponding to the user
function get_login_times() {
	require("inc/database.php");
	
	$table_name = $_SESSION["user_name"]."_findings";
	
	try {
		$results = $db->query("
			SELECT 		count
			FROM 		$table_name
			ORDER BY 	id 	DESC
			LIMIT 		1
		");
	} catch (Exception $e) {
		echo "Cannot connect to database: get_login_times().";
		print_r( $db->errorinfo());
		exit;
	}
	
	return intval($results->fetchColumn(0));
}


// Starting point of the new session, the login time or 'count' value would be increased by 1
function new_session() {
	
	require("inc/database.php");
	
	$count = get_login_times();	
	$count += 1;

	// Adding new session, login time '$count' is increased by 1

	date_default_timezone_set('Asia/Bangkok');
	$date 			= 	date('l jS \of F Y h:i:s A');

	$new_session = "NEW SESSION";
	
	$table_name = $_SESSION["user_name"]."_findings";
	

	
	try {
		$results = $db->query("
			INSERT INTO $table_name	
			(`index`,
			`items`,
			`ranks`,
			`descriptions`,
			`remarks`,
			`status`,
			`responsibility`,
			`count`,
			`date_login`,
			`date_close`,
			`recording`
			)
			VALUES (
			'$new_session',
			'$new_session',
			'0',
			'$new_session',
			'$new_session',
			'$new_session',
			'2',
			'$count',
			'$date',
			'$new_session',
			'$new_session'
			)
		");
	} catch (Exception $e) {
		echo "Cannot connect to database: new_session() (2).";
		print_r( $db->errorinfo());
		exit;
	}
	
}

/*
// To translate to Vietnamese

function translate_to_vietnamese( $label ) {

	require("inc/database.php");
	
	try {
		$results = $db->query("
			SELECT 		vietnam
			FROM 		language_translate
			WHERE 		label = '$label'
		");
	} catch (Exception $e) {
		echo "Cannot connect to database: translate_to_vietnamese().";
		print_r( $db->errorinfo());
		exit;
	}

	return $results->fetchColumn(0);
}

function count_columns($array) {
	
	foreach ($array as $row) {
		$columns = count($row);
	}
	return $columns;
}

*/

// Create tables corresponding to users
function create_user_table_findings() {
	
	require("inc/database.php");
	
	$table_name = $_SESSION["user_name"]."_findings";
	
	$query = "
		CREATE TABLE IF NOT EXISTS $table_name (
			`id` int(22) auto_increment,
			`index` varchar(255) COLLATE utf8_unicode_ci,
			`items` varchar(255) COLLATE utf8_unicode_ci,
			`ranks` int(3),
			`descriptions` varchar(255) COLLATE utf8_unicode_ci,
			`remarks` varchar(255) COLLATE utf8_unicode_ci,
			`status` varchar(255) COLLATE utf8_unicode_ci,
			`responsibility` tinyint(1),
			`count` int(11),
			`date_login` varchar(255) COLLATE utf8_unicode_ci,
			`date_close` varchar(255) COLLATE utf8_unicode_ci,
			`recording` varchar(255) COLLATE utf8_unicode_ci,
			PRIMARY KEY (id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	";
	
	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: findings.";
		//print_r( $db->errorinfo());
		exit;
	}	
}

function create_user_table_siteinfos() {
	
	require("inc/database.php");
	
	$table_name = $_SESSION["user_name"]."_siteinfos";
	
	$query = "
		CREATE TABLE IF NOT EXISTS $table_name (
			`count` int(11),
			`site_name` varchar(255) COLLATE utf8_unicode_ci,
			`lift` varchar(255) COLLATE utf8_unicode_ci,
			`sup` varchar(255) COLLATE utf8_unicode_ci,
			`EI_tech_1` varchar(255) COLLATE utf8_unicode_ci,
			`EI_tech_2` varchar(255) COLLATE utf8_unicode_ci,
			`reinspection` varchar(255) COLLATE utf8_unicode_ci,
			`equip_no` varchar(255) COLLATE utf8_unicode_ci,
			`MWC` varchar(255) COLLATE utf8_unicode_ci,
			`sales_office` varchar(255) COLLATE utf8_unicode_ci,
			`date_check` varchar(255) COLLATE utf8_unicode_ci,
			PRIMARY KEY (count)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	";
	
	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: siteinfos.";
		//print_r( $db->errorinfo());
		exit;
	}	
}

function create_user_table_scoring() {
	
	require("inc/database.php");
	
	$table_name = $_SESSION["user_name"]."_scoring";
	
	$query = "
		CREATE TABLE IF NOT EXISTS $table_name (
			`count` int(11),
			`MUS` int(3),
			`MFS` int(3),
			`total_score` int(3),
			PRIMARY KEY (count)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	";
	
	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: siteinfos.";
		//print_r( $db->errorinfo());
		exit;
	}	
}

// Showing MQI findings summary
function get_monitors_info($user_name, $sales_office_search, $MWC_search, $status_search, $remark_search, $date_from, $date_to, $show_amount) {

	$table_findings = $user_name."_findings";
	$table_siteinfos= $user_name."_siteinfos";

	$sales_term = "";
	if (!empty($sales_office_search)) {
		foreach ($sales_office_search as $sale_office) {		
				$sales_term .= " OR S.sales_office = '$sale_office' ";
		}
		$sales_term = substr($sales_term, 3);
		$sales_term = " AND ( ". $sales_term ." ) ";
	}

	$MWC_term = "";
	if (!empty($MWC_search)) {
		foreach ($MWC_search as $MWC) {
				$MWC_term .= " OR S.MWC = '$MWC' ";
		}
		$MWC_term = substr($MWC_term, 3);
		$MWC_term = " AND ( ". $MWC_term ." ) ";
	}

	$status_term = "";
	if (!empty($status_search)) {
		foreach ($status_search as $status) {		
				$status_term .= " OR F.status = '$status' ";
		}
		$status_term = substr($status_term, 3);
		$status_term = " AND ( ". $status_term ." ) ";
	}

	$remark_term = "";
	if (!empty($remark_search)) {
		foreach ($remark_search as $remark) {	
		// check if remarks are High Potent. Risk NC or Low Potent. Risk NC, there are 2 options to search for	
				if ( $remark == 'High Potent. Risk NC' ) {
					$remark_term .= " OR F.remarks = '$remark' ";
					$remark_term .= " OR F.remarks = 'High Potential Risk NC' ";
				} elseif ( $remark == 'Low Potent. Risk NC' ) {
					$remark_term .= " OR F.remarks = '$remark' ";
					$remark_term .= " OR F.remarks = 'Low Potential Risk NC' ";
				} else {
					$remark_term .= " OR F.remarks = '$remark' ";
				}
		}
		$remark_term = substr($remark_term, 3);
		$remark_term = " AND ( ". $remark_term ." ) ";
	}

	$date_from_term = "";
	if (!empty($date_from)) {
		$date_from_term = " AND date_check >= '$date_from' ";
	}

	$date_to_term = "";
	if (!empty($date_to)) {
		$date_to_term = " AND date_check <= '$date_to' ";
	}

	$amount_term = "";
	if (!empty($show_amount)) {
		$amount_term = " LIMIT $show_amount ";
	}

	require("inc/database.php");

	$query = "
		SELECT 	S.date_check, 
			   	S.MWC, 
			   	S.sales_office, 
			   	S.EI_tech_1, 
			   	S.EI_tech_2, 
			   	S.site_name, 
			   	S.equip_no, 
			   	S.lift,
			  	F.id,
			   	F.index,
			   	F.descriptions,
			   	F.remarks,
			   	F.status,
			   	F.date_close
		FROM   	$table_siteinfos AS S,
			   	$table_findings  AS F
		WHERE  	S.count = F.count 
				" .$sales_term. " 
				" .$MWC_term. "
				" .$status_term. "
				" .$remark_term. "
				" .$date_from_term. "
				" .$date_to_term. "
		AND    	F.descriptions != 'NEW SESSION'
		ORDER BY S.date_check DESC
				" .$amount_term. "
			";

	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: get_monitors_info";
		print_r( $db->errorinfo());
		exit;
	}	

	$results = $results->fetchAll(PDO::FETCH_ASSOC);

	// Adding the key "user" to the array $results
	foreach ($results as $key=>$result) {
		$results["$key"]["username"] = $user_name;
	}

	return $results;

}

// Showing MQI findings summary
function get_MQI_results ( $user_name, $sales_office_search, $MWC_search, $date_from, $date_to, $show_amount ) {

	$table_siteinfos 	= $user_name."_siteinfos";
	$table_scoring 		= $user_name."_scoring";

	$sales_term = "";
	if (!empty($sales_office_search)) {
		foreach ($sales_office_search as $sale_office) {		
				$sales_term .= " OR S.sales_office = '$sale_office' ";
		}
		$sales_term = substr($sales_term, 3);
		$sales_term = " AND ( ". $sales_term ." ) ";
	}


	$MWC_term = "";
	if (!empty($MWC_search)) {
		foreach ($MWC_search as $MWC) {
				$MWC_term .= " OR S.MWC = '$MWC' ";
		}
		$MWC_term = substr($MWC_term, 3);
		$MWC_term = " AND ( ". $MWC_term ." ) ";
	}

	$date_from_term = "";
	if (!empty($date_from)) {
		$date_from_term = " AND date_check >= '$date_from' ";
	}

	$date_to_term = "";
	if (!empty($date_to)) {
		$date_to_term = " AND date_check <= '$date_to' ";
	}

	$amount_term = "";
	if (!empty($show_amount)) {
		$amount_term = " LIMIT $show_amount ";
	}

	require("inc/database.php");

	$query = "

		SELECT 	S.count,
				S.MWC,
				S.sales_office,
				S.site_name,
				S.equip_no,
				S.lift,
				S.date_check,
				S.reinspection,
				SC.MFS,
				SC.MUS,
				SC.total_score
		FROM 	$table_siteinfos AS S,
				$table_scoring AS SC
		WHERE 	S.count = SC.count
				" .$sales_term. " 
				" .$MWC_term. "
				" .$date_from_term. "
				" .$date_to_term. "
		ORDER BY S.date_check DESC
				" .$amount_term. "

			";

	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: get_MQI_results";
		//print_r( $db->errorinfo());
		exit;
	}	

	$results = $results->fetchAll(PDO::FETCH_ASSOC);

	// Adding the key "user" to the array $results
	foreach ($results as $key=>$result) {
		$results["$key"]["username"] = $user_name;
	}

	return $results;

}

// Get number of critical items
function number_of_critical_items($user, $count, $critical_item) {

	$user_table = $user."_findings";

	require("inc/database.php");

	$query = "

		SELECT COUNT(*)
		FROM 	$user_table
		WHERE 	count = $count
		AND 	remarks = '$critical_item'

	";

	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: number_of_critical_items";
		print_r( $db->errorinfo());
		exit;
	}	

	$results = $results->fetchColumn(0);

	return $results;

}

// Get all usernames
function get_all_users(){

	require("inc/database.php");

	$query = "

		SELECT 	username
		FROM 	users

	";

	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: get_all_users";
		//print_r( $db->errorinfo());
		exit;
	}	

	$results = $results->fetchAll(PDO::FETCH_ASSOC);

	return $results;

}

// Get usernames depending on roles
function get_role_users( $user_role ){

	require("inc/database.php");

	$query = "

		SELECT 	username
		FROM 	users
		WHERE 	user_role = '$user_role'

	";

	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: get_role_users";
		print_r( $db->errorinfo());
		exit;
	}	

	$users_list = $results->fetchAll(PDO::FETCH_ASSOC);

	$i = 0;
	foreach ( $users_list as $user ) {
		$users_array[$i] = $user["username"];
		$i++; 
	}

	return $users_array;

}

// From username to get password
function get_pass_from_user( $username ) {
	require("inc/database.php");
	try {
		$results = $db->prepare("
				SELECT 	`password`
				FROM 	users
				WHERE 	username = ?
			");
		$results->bindParam(1, $username);
		$results->execute();
	} catch (Exception $e) {
		echo "Không thể kết nối đến database trong function get_pass_from_user()";
		print_r( $db->errorinfo() );
		exit;
	}
	$password = $results->fetchColumn(0);
	return $password;
}

// Getting status values corresponding to $id, $user
function get_finding_status($id, $user){

	$user_table = $user."_findings";

	require("inc/database.php");

	$query = "

		SELECT 	status
		FROM 	$user_table
		WHERE 	id = $id

	";

	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: get_finding_status";
		//print_r( $db->errorinfo());
		exit;
	}	

	$results = $results->fetchColumn(0);

	return $results;
}

// Getting records corresponding to $id, $user
function get_finding_record($id, $user){

	$user_table = $user."_findings";

	require("inc/database.php");

	$query = "

		SELECT 	recording
		FROM 	$user_table
		WHERE 	id = $id

	";

	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: get_finding_status";
		//print_r( $db->errorinfo());
		exit;
	}	

	$results = $results->fetchColumn(0);

	return $results;
}


// Updating the status corresponding to $id and $user_table
function update_finding_status($id, $user, $status, $current_record, $new_record) {

	$user_table = $user."_findings";

	$record = $current_record." | ".$new_record;

	require("inc/database.php");

	try {
		$results = $db->prepare("
			UPDATE $user_table 
			SET status 		= ?,
				recording 	= ?
			WHERE id 		= $id
			");
		$results->bindParam(1,$status);
		$results->bindParam(2,$record);
		$results->execute();
	} catch (Exception $e) {
		echo "Cannot connect to database: update_finding_status";
		print_r( $db->errorinfo());
		exit;
	}	

}

//Finding date check
function finding_date_check($user, $id) {

	require("inc/database.php");

	$user_findings = $user."_findings";

	try {
		$results = $db->query("
			SELECT 	count 
			FROM 	$user_findings
			WHERE 	id = $id
			");
	} catch (Exception $e) {
		echo "Cannot connect to database: finding_date_check 1";
		print_r( $db->errorinfo());
		exit;
	}	

	$count = $results->fetchColumn(0);

	$user_site = $user."_siteinfos";
	try {
		$results = $db->query("
			SELECT 	date_check
			FROM 	$user_site
			WHERE 	count = $count
			");
	} catch (Exception $e) {
		echo "Cannot connect to database: finding_date_check 2";
		print_r( $db->errorinfo());
		exit;
	}
	
	return $date_check = $results->fetchColumn(0);

}

// Update date close in monitors.php
function update_date_close($user, $id, $date_close) {

	//calculate the gap days between date_close and day_inspection
	$date_check = finding_date_check($user, $id);
	$date_check_time = strtotime($date_check);
	$date_close_time = strtotime($date_close);
	$datediff = $date_close_time - $date_check_time;
	$days = $datediff/(60*60*24);

	$user_table = $user."_findings";

	require("inc/database.php");

	try {
		$results = $db->prepare("
			UPDATE $user_table 
			SET date_close 	= ?
			WHERE id 		= $id
			");
		$results->bindParam(1,$days);
		$results->execute();
	} catch (Exception $e) {
		echo "Cannot connect to database: update_finding_status";
		//print_r( $db->errorinfo());
		exit;
	}	
}

//Scoring rule MUS
function MUS_scoring_rule($rank, $weight) {

	if ($rank == -3 || $rank == -2 || $rank == -1) {
		$score = $rank * $weight;
	} elseif ($rank == 1 || $rank == 1.5 || $rank == 0) {
		$score = $rank;
	} else {
		echo "Something wrong with the MUS_scoring_rule in function.php";
		exit;
	}

	return $score;
}


//Scoring rule MFS
function MFS_scoring_rule($rank, $weight, $responsibility) {

	if ($responsibility == 1) {
		$score = 1;
	} else {
		if ($rank == -3 || $rank == -2 || $rank == -1) {
			$score = $rank * $weight;
		} elseif ($rank == 1 || $rank == 1.5 || $rank == 0) {
			$score = $rank;
		} else {
			echo "Something wrong with the MFS_scoring_rule in function.php";
			exit;
		}
	}

	return $score;
}

//Scoring rule Total_Score
function Total_scoring_rule($rank) {

	if ($rank == 0) {
		$score = 0;
	} else {
		$score = 1;
	}

	return $score;
}

//Putting score to the database username_scoring table
function score_database ($table, $count, $MUS_score, $MFS_score, $Total_score) {

	require("inc/database.php");

	$query = "

		INSERT INTO $table
		VALUES ('$count','$MUS_score','$MFS_score','$Total_score')

	";

	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "FAIL TO INSERT INTO DATABASE score_database";
		print_r( $db->errorinfo());
		exit;
	}
}

//Revoke the fullname of the user
function get_user_fullname($user_id) {

	require("inc/database.php");

	$query = "

		SELECT 	fullname
		FROM 	users
		WHERE 	id = $user_id

	";

	try {
		$results = $db->query($query);
	} catch (Exception $e) {
		echo "Cannot connect to database: get_user_fullname";
		//print_r( $db->errorinfo());
		exit;
	}	

	$results = $results->fetchColumn(0);

	return $results;

}

// Calculate the days gap between inspection date and close date
function days_gap_inspec_to_close($id, $user, $date_close) {

	$date_check = finding_date_check($user, $id);
	$date_check_time = strtotime($date_check);
	$date_close_time = strtotime($date_close);
	$datediff = $date_close_time - $date_check_time;
	return $days = $datediff/(60*60*24);

}

// Using in sorting arrays-dates in monitors.php
function date_compare($a, $b) {

    $t1 = strtotime($a["date_check"]);
    $t2 = strtotime($b["date_check"]);
    return $t2 - $t1;

}    

// Get the password <-> username
function get_the_password( $username ) {
	require("inc/database.php");

	try {
		$results = $db->query("
			SELECT 	`password`
			FROM 	users
			WHERE 	username = '$username'
			");
	} catch (Exception $e) {
		echo "Không thể kết nối database ở mục: get_user_password()";
		print_r( $db->errorinfo() );
		exit;
	}

	return $password = $results->fetchColumn(0);
}

//Sending Email
function send_mail($from, $to, $cc, $reply, $subject, $body) {
    require_once ('inc/phpMailer/class.phpmailer.php');
    require_once ('inc/phpMailer/class.smtp.php');

    $mail = new PHPMailer(); // create a new object
    $mail->CharSet = "UTF-8";
    $mail->IsSMTP(); // enable SMTP
    $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true; // authentication enabled
    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 465; // or 587
    $mail->IsHTML(true);
    $mail->Username = "SchindlerMQI@gmail.com";
    $mail->Password = "SchindlerMQI1";
    $mail->setFrom($from);
    $mail->addReplyTo($reply);
    $mail->Subject = $subject;
    $mail->Body = $body;

    $mail->AddAddress($to);
    $mail->addCC($cc);

    //$mail->AddEmbeddedImage('css/images/slogan.png', 'slogan-idabong.com');
     if($mail->Send())
        {
        return TRUE; //If email is sent 
        }
        else
        {
        return FALSE; //If email is NOT sent 
        }
}//END Sending Email

// Get 

?>

