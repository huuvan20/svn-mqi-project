<?php

$message="";

if( isset($_POST['user_name']) && isset($_POST['password']) ) {

	$username = $_POST['user_name'];
	$password = $_POST['password'];

	$password_hash = md5($password);

	if ( !empty($username) && !empty($password) ) {

		require("inc/database.php");

		try {
			$results = $db->prepare("
				SELECT 	`id`, `username`, `user_role`
		    	FROM 	users 
		    	WHERE 	username =? 
		    	AND 	password =? 
			");
			$results->bindParam(1,$username);
			$results->bindParam(2,$password_hash);
			$results->execute();
		} catch (Exception $e) {
			echo "Cannot connect to database: login";
			print_r( $db->errorinfo());
			exit;
		}

		$row = $results->fetchAll(PDO::FETCH_ASSOC);

		if( !empty($row[0]["id"]) && !empty($row[0]["username"]) && !empty($row[0]["user_role"]) ) {
			$_SESSION["user_id"] = $row[0]["id"];
			$_SESSION["user_name"] = $row[0]["username"];
			$_SESSION["user_role"] = $row[0]["user_role"];

			//Create database tables if not existed
			create_user_table_findings();
			if ( $_SESSION["user_role"] == 'mqi' ) {
				create_user_table_siteinfos();
				create_user_table_scoring();
			}
		
			new_session();
			header('Location: index.php');
		} else {
			$message = "Username hoặc Password không chính xác!";
		}	

	} else {
		$message = "Xin nhập Username và Password";
	}
}

?>

<form name="formUser" method="post" action="<?php echo $current_file; ?>">

<div class="login-form-whole">

	<div>
		<?php if( !empty($message) ) {
				 echo '<p class="login-message">' .$message. '</p>'; 
				} 
		?>
	</div>

	<div class="login-form-div">
		<table class="login-form">
			<tr>
				<td><input type="text" name="user_name" class="panel-body-date login-field"></td>
			</tr>
			<tr>
				<td><input type="password" name="password" class="panel-body-date login-field"></td>
			</tr>
			<tr class="tablefooter">
				<td><input type="submit" name="submit" value="ĐĂNG NHẬP" class="button login-button"></td>
			</tr>
		</table>
	</div>

</div>

</form>
