<?php

require_once("inc/config.php");
require("inc/functions.php");
require("items/items.php");

include("inc/session_timeout.php");

if ( loggedin() ) {

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MQI | Schindler VN</title>
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" href="css/normalize.css">
	<link href='http://fonts.googleapis.com/css?family=Changa+One|Open+Sans:400,400italic,700,700italic,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
	<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="css/main.css">

</head>
<body>

<!-- HEADER AND MENU NAVIGATION -->
	<header>
		<a href="index.php" id="logo">
	    	<h1>MAINTENANCE QUALITY INSPECTION</h1>
	    	<h2>Schindler VN</h2>
		</a>
     	<nav>
	        <ul>
	          <li><a href="index.php">Home</a></li>
	          <li><a href="form.php">Điền MQI</a></li>
	          <li><a href="monitors.php">Các findings</a></li>
	          <li><a href="MQI_results.php" class="selected">Kết quả</a></li>
	        </ul>
    	</nav>
	</header>

<!--++++++++++++++++++++++++ SEARCH FORM +++++++++++++++++++++++++++++++-->
<form action="<?php echo $current_file; ?>" method="post" class="monitors-form">

	<div class="monitors-div-search">
		<ul class="panel-group">

			<!-- SALE OFFICE OPTIONS -->
			
            <li class="panel-body">
                <select id="sales_office_search" name="sales_office_search[]" multiple="multiple">
					<?php 
						foreach ($sales_office_list as $sales_office) { 
					?>
					<option value="<?php echo $sales_office[0]; ?>"
						<?php
							if ( isset($_POST["sales_office_search"]) && !empty($_POST["sales_office_search"]) ) {
								if ( in_array($sales_office[0], $_POST["sales_office_search"]) ) {
									echo " selected=\"selected\" ";
								}
							}
						?>
					><?php echo $sales_office[0]; ?></option>
					<?php } ?>
				</select>
            </li>

            <!-- MWC OPTIONS -->
		
            <li class="panel-body">
                <select id="MWC_search" name="MWC_search[]" multiple="multiple">
					<?php 
						foreach ($MWC_list as $MWC) { 
					?>
					<option value="<?php echo $MWC; ?>"
						<?php
							if ( isset($_POST["MWC_search"]) && !empty($_POST["MWC_search"]) ) {
								if ( in_array($MWC, $_POST["MWC_search"]) ) {
									echo " selected=\"selected\" ";
								}
							}
						?>
					><?php echo $MWC; ?></option>
					<?php } ?>
				</select>
            </li>

      		<!-- DATE FROM SEARCH -->
			<li class="panel-body">
				<input type="text" id="from" name="date_from" value="<?php
					if ( isset($_POST["date_from"]) ) {
						echo $_POST["date_from"];
					}
				 ?>" class="panel-body-date" placeholder="From">
			</li>

            <!-- DATE TO SEARCH -->
			<li class="panel-body">
				<input type="text" id="to" name="date_to" value="<?php
					if ( isset($_POST["date_to"]) ) {
						echo $_POST["date_to"];
					}
				 ?>" class="panel-body-date" placeholder="To">
			</li>

	        <!-- SHOW AMOUNT OF SEARCHING -->
	        <li class="panel-body">
	    		<select name="show_amount" class="panel-body-date show-amount-class">
	    			<?php foreach ($show_amount_list as $show_amount) { ?>
	    			<option value=<?php echo $show_amount; ?> 
	    				<?php
							if ( isset($_POST["show_amount"]) && !empty($_POST["show_amount"]) ) {
								if ( $_POST["show_amount"] == $show_amount ) {
									echo " selected=\"selected\" ";
								}
							}
						?>
	    			>
	    				<?php 
	    				if ( empty($show_amount) ) {
	    					echo "ALL"; 
	    				} else {
	    					echo $show_amount;
	    				}
	    				?>
	    			</option>
	    			<?php } ?>
	    		</select>
	    	</li>
	 
		</ul>
	</div>
			
	<input type="hidden" name="search" value="update">
	<input type="submit" value="Tìm kiếm" class="button">
</form>
<!--+++++++++++++++++++++++++ CLOSE THE SEARCH FORM +++++++++++++++++++++++++++++++++-->


<!--+++++++++++++++++ LISTING OUT ALL THE SEARCHING RESULTS OF FINDINGS +++++++++++++++++-->
<?php 
	if (isset($_POST["search"]) && $_POST["search"] == "update") { ?>
	<table class="monitors-results">
		<!-- TABLE TITLE -->
		<tr>
			<th>MWC</th>
			<th>Sale Office</th>
			<th>Project</th>
			<th>Equip. No.</th>
			<th>Lift No.</th>
			<th>Insp. Date</th>
			<th>Critical Find.</th>
			<th>Fitter Score</th>
			<th>Unit Score</th>
			<th>Final Result</th>
			<th>Re-Insp.</th>
		</tr>

<!--++++++++++ GET THE SEARCH DESIRED VALUES AND SEARCHING INSIDE THE DATABASE +++++++++++-->
<?php 
	if(isset($_POST["search"]) && $_POST["search"] == "update") {

		if ( isset($_POST["sales_office_search"]) ) {
			$sales_office_search 	= $_POST["sales_office_search"];
		} else {
			$sales_office_search 	= "";
		}

		if ( isset($_POST["MWC_search"]) ) {
			$MWC_search				= $_POST["MWC_search"];
		} else {
			$MWC_search 			= "";
		}

		if ( isset($_POST["date_from"]) && !empty($_POST["date_from"]) ) {
			$date_from = $_POST["date_from"];
			$date_from = date('Y-m-d', strtotime($date_from));
		} else {
			$date_from 	= "";
		}

		if ( isset($_POST["date_to"]) && !empty($_POST["date_to"]) ) {
			$date_to = $_POST["date_to"];
			$date_to = date('Y-m-d', strtotime($date_to));
		} else {
			$date_to 	= "";
		}

		$show_amount 	= $_POST["show_amount"];

		$mqi_users_list = get_role_users( 'mqi' );
		$i = 0;
		foreach ($mqi_users_list as $user) {
			
			$user_MQI_results = get_MQI_results($user, $sales_office_search, $MWC_search, $date_from, $date_to, $show_amount);

			foreach ($user_MQI_results as $MQI_result_elem) {
				$MQI_result_array[$i] = $MQI_result_elem;
				$i++;
			}
		}

		// Sort the array values by dates
		if ( !empty($MQI_result_array) ) {	

			usort($MQI_result_array, 'date_compare');

			$j = 0;

			foreach ($MQI_result_array as $MQI_result) {

				$j++;
				if ( $j <= $show_amount || empty($show_amount) ) {

				$number_of_critical_items = number_of_critical_items($MQI_result["username"], $MQI_result["count"], $critical_item);	

			?>
			<!-- SHOWING ALL THE DESIRED RESULTS IN THE TABLE -->
			<tr> 
				<!-- MWC, SALE OFFICE, SITE NAME, EQUIPMENT NO, LIFT NO, DATE INSPECTION, NO CRITICAL ITEMS, MFS, MUS, RESULTS -->
				<td class="monitors-highlight"><?php echo $MQI_result["MWC"] ?></td>
				<td class="monitors-highlight"><?php echo $MQI_result["sales_office"] ?></td>
				<td><?php echo $MQI_result["site_name"] ?></td>
				<td><?php echo $MQI_result["equip_no"] ?></td>
				<td><?php echo $MQI_result["lift"] ?></td>
				<td><?php
					$MQI_date_check = date("d/m/y", strtotime($MQI_result["date_check"]));
					echo $MQI_date_check;
					?></td>
				<td><?php echo $number_of_critical_items; ?></td>
				<td><?php 
					$MFS = 100 * $MQI_result["MFS"] / $MQI_result["total_score"];
					echo round($MFS, 2)."%"; 
				?></td>
				<td><?php
					$MUS = 100 * $MQI_result["MUS"] / $MQI_result["total_score"];
					echo round($MUS, 2)."%";
				?></td>
				
				<?php
				if ( $number_of_critical_items > 0 || $MFS < 80 || $MUS <80 ) {
					echo "<td class=\"fail-decor\">";
				} else {
					echo "<td class=\"monitors-highlight\">";
				}
				?>

				<?php
					if ( $number_of_critical_items > 0 || $MFS < 80 || $MUS <80 ) { echo "FAIL"; } else { echo "PASS"; }
				?>
				</td>

				<?php 
					if ( $MQI_result["reinspection"] == 1 ) {
						echo "<td class=\"reinspec-no\" >";
					} elseif ( $MQI_result["reinspection"] == 2 ) {
						echo "<td class=\"reinspec-yes\" >";
					} else {
						echo "<td>";
					}			  
				?>
					<?php 
						if ( $MQI_result["reinspection"] == 1 ) {
							echo "No";
						} elseif ( $MQI_result["reinspection"] == 2 ) {
							echo "Yes";
						}
						  
					?>
				</td>
			</tr>
			<?php 
				} 
			} 
		} 
	} 
	?>
	</table>
	
<?php } ?>

	<footer>
		<p><a href="logout.php">Đăng xuất</a></p>
		<p>&copy; 2015 Schindler VN</p>
	</footer>
	
	<!-- jQuery and Boostrap included -->
	<script src="http://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- Bootstrap multiselect jQuery -->
	<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
	<!-- UI JQUERY PLUGIN (DATEPICKER) -->
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<!-- MAIN JS -->
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>

<?php 

} else {
	header("Location:index.php");
}

?>