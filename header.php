<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <title>MQI | Schindler VN</title>
	    <meta name="viewport" content="width=device-width">
	    <link rel="stylesheet" href="css/normalize.css">
	    <link href='http://fonts.googleapis.com/css?family=Changa+One|Open+Sans:400,400italic,700,700italic,800' rel='stylesheet' type='text/css'>
	    <link rel="stylesheet" href="css/main.css">
	</head>
	<body id="main">
		<header>
			<a href="index.php" id="logo">
	    	<h1>MAINTENANCE QUALITY INSPECTION</h1>
	    	<h2>Schindler VN</h2>
		  </a>
     	<nav>
        <ul>
          <li><a href="index.php" class="selected">Home</a></li>
          <li><a href="form.php">Điền MQI</a></li>
          <li><a href="monitors.php">Các findings</a></li>
          <li><a href="MQI_results.php">Kết quả</a></li>
        </ul>
    	</nav>
		</header>
		<div id="wrapper">