<?php

require_once("inc/config.php");

require("inc/functions.php");
require("items/items.php");

// -----------------------Put to database site infos----------------------------------------

if (isset($_POST["site_name"]) 
		&& isset($_POST["lift"]) 
		&& isset($_POST["sup"]) 
		&& isset($_POST["EI_tech_1"]) 
		&& isset($_POST["reinspection"]) 
		&& isset($_POST["equip_no"])
		&& isset($_POST["MWC"]) 
		&& isset($_POST["sales_office"])
		&& isset($_POST["date_check"])) {

	$count 		= get_login_times();
	$site_name 	= $_POST["site_name"];
	$lift 		= $_POST["lift"];
	$sup		= $_POST["sup"];
	$EI_tech_1	= $_POST["EI_tech_1"];
	$reinspection = $_POST["reinspection"];
	$equip_no	= $_POST["equip_no"];
	$MWC		= $_POST["MWC"];
	$sales_office = $_POST["sales_office"];
	$date_check = $_POST["date_check"];

	$reinspection_YN = "Không";
	if ($reinspection == 2) {
		$reinspection_YN = "Có";
	}

	if (!empty($site_name)
		&& !empty($lift)
		&& !empty($sup)
		&& !empty($EI_tech_1)
		&& !empty($reinspection)
		&& !empty($equip_no)
		&& !empty($MWC)
		&& !empty($sales_office)
		&& !empty($date_check)) {

		// Body of the email
		$email_body = "
		<!DOCTYPE html>
		<html>
			<head>
			</head>
			<body>
			<div class='site'>
				<div class='site-info'>
					<div class='column'>
						<p>
							Tên công trình: ".$site_name."
						</p>
						<p>
							Tên thang: ".$lift."
						</p>
						<p>
							Giám sát: ".$sup."
						</p>
					</div>
					<div class='column'>
						<p>
							Nhân viên EI: ".$EI_tech_1."
						</p>
						<p>
							Đã kiểm tra: ".$reinspection_YN."
						</p>
						<p>
							Số thiết bị: ".$equip_no."
						</p>
					</div>
					<div class='column'>
						<p>
							Số MWC: ".$MWC."
						</p>
						<p>
							Số Sales Office: ".$sales_office."
						</p>
						<p>
							Ngày kiểm tra: ".$date_check."
						</p>
					</div>
				</div>
				<div class='site-findings'>
				<table style='border: 1px solid silver; width: 100%;'>
					<tr style='border: 1px solid silver;'>
						<th>Mục</th>
						<th>Mô tả</th>
						<th>Ghi chú</th>
						<th>Tình trạng</th>
						<th>Đánh giá</th>
					</tr>
			
		";


		// PUT TO DATABASE SITEINFO
		require("inc/database.php");

		$table_name = $_SESSION["user_name"]."_siteinfos";

		$EI_tech_2 = ""; // Just for backup of EI Tech 2
		$date_check = date('Y-m-d', strtotime($date_check));

		try {
			$results = $db->prepare("
				INSERT INTO $table_name
				VALUES 	( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )
				");
			$results->bindParam(1,$count);
			$results->bindParam(2,$site_name);
			$results->bindParam(3,$lift);
			$results->bindParam(4,$sup);
			$results->bindParam(5,$EI_tech_1);
			$results->bindParam(6,$EI_tech_2);
			$results->bindParam(7,$reinspection);
			$results->bindParam(8,$equip_no);
			$results->bindParam(9,$MWC);
			$results->bindParam(10,$sales_office);
			$results->bindParam(11,$date_check);
			$results->execute();

		} catch (Exception $e) {
			echo "FAIL TO INSERT INTO DATABASE FROM THE SITEINFOS ";
			print_r($db->errorinfo());
			exit;
		}
	}
}
// -------------------------Finish put to database site infos----------------------------------------


// --------------------------Put to database findings details----------------------------------------
$i = 0;

$MUS_score = 0;
$MFS_score = 0;
$Total_score = 0;
$number_risk_NC	= 0;

foreach ($items_list as $item_section) {

	foreach ($item_section as $item) {
	$i++;

	$rank 			= 	$_POST["rank"][$i];
	$description 	= 	$_POST["description"][$i];
	$remark 		= 	$_POST["remark"][$i];
	$status 		= 	$_POST["status"][$i];
	$responsibility = 	$_POST["responsibility"][$i];

	date_default_timezone_set('Asia/Bangkok');
	$date_login 	= 	date('l jS \of F Y h:i:s A');

	$count 			= 	get_login_times();

	// Check to record the findings
	if ($rank == -1 || $rank == -2 || $rank == -3 || !empty($description) || !empty($remark) || !empty($status) ) {

	//Number of risk NC
		if ($remark == "Immediate Risk NC") {
			$number_risk_NC += 1;
		}

	// Mail details
		$email_body .= "
				<tr style='border: 1px solid silver;'>
					<td style='border: 1px solid silver;'>".$item[0]."</td>
					<td style='border: 1px solid silver; width: 40%;'>".$description."</td>
					<td style='border: 1px solid silver;'>".$remark."</td>
					<td style='border: 1px solid silver;'>".$status."</td>
					<td style='border: 1px solid silver;'>".$rank."</td>
				</tr>
		";


	//---------------Put to database findings that rank < -1, -2, -3 >--------------------
		require("inc/database.php");
		
		$table_name = $_SESSION["user_name"]."_findings";

		if ($status == "Done") {
			$date_close = 0;
		} else {
			$date_close = "";
		}

		try {
			$results = $db->prepare("
				INSERT INTO 	$table_name 
								(`index`,
								`items`,
								`ranks`,
								`descriptions`,
								`remarks`,
								`status`,
								`responsibility`,
								`count`,
								`date_login`,
								`date_close`,
								`recording`)

				VALUES 			(	:index, 
									:items, 
									:ranks, 
									:descriptions, 
									:remarks, 
									:status, 
									:responsibility, 
									:count, 
									:date_login, 
									:date_close, 
									'' 	)
				");
			$results->bindParam(':index',$item[0]);
			$results->bindParam(':items',$item[1]);
			$results->bindParam(':ranks',$rank);
			$results->bindParam(':descriptions',$description);
			$results->bindParam(':remarks',$remark);
			$results->bindParam(':status',$status);
			$results->bindParam(':responsibility',$responsibility);
			$results->bindParam(':count',$count);
			$results->bindParam(':date_login',$date_login);
			$results->bindParam(':date_close',$date_close);
			$results->execute();

		} catch (Exception $e) {
			echo "FAIL TO INSERT INTO DATABASE FROM THE FORM: FINDINGS";
			print_r( $db->errorinfo());
			exit;
		}
			
	}


	//---------------------Scoring the technician and the site-----------------------

	$MUS_item_score = MUS_scoring_rule($rank, $item[2]);
	$MUS_score = $MUS_score + $MUS_item_score;

	$MFS_item_score = MFS_scoring_rule($rank, $item[2], $responsibility);
	$MFS_score = $MFS_score + $MFS_item_score;

	$Total_item_score = Total_scoring_rule($rank);
	$Total_score = $Total_score + $Total_item_score;

	}

}

// ----------------------------------Put site's score to database------------------------------------

$score_table 	= $_SESSION["user_name"]."_scoring";

$count 			= get_login_times();

score_database($score_table, $count, $MUS_score, $MFS_score, $Total_score);

//Score of the site in the email
$MUS_score_per = round($MUS_score*100/$Total_score, 1);
$MFS_score_per = round($MFS_score*100/$Total_score, 1);
if ($MUS_score_per < 80 || $MFS_score_per < 80 || $number_risk_NC != 0) {
	$mail_score = "KHÔNG ĐẠT";
} else {
	$mail_score = "ĐẠT";
}
$email_body .= "
			</table>
			</div>
			<div class='site-result'>
				<p> Điểm công trình: ".$MUS_score_per."</p>
				<p> Điểm nhân viên EI: ".$MFS_score_per."</p>
			";
if ($number_risk_NC != 0) {
	$email_body .= "<p>Hạng mục nguy hiểm cần khắc phục ngay: ".$number_risk_NC."</p>";
}
$email_body .=	"<p style='font-weight: bold;'> KẾT QUẢ: ".$mail_score."</p>
				</div>
			</div>
		</body>
	</html>
";

// Title of the email
$email_title = "[MQI] Công trình ".$site_name;

// Check email's SL corresponding to sale office number
foreach ($sales_office_list as $sales_office_item) {
	if ($sales_office_item[0] == $sales_office) {
		$cc = $sales_office_item[1];
	} else {
		$cc = "";
	}
}

// Check email's site supervisor coresponding to his name
foreach ($sups_list as $sup_item) {
	if ($sup_item[0] == $sup) {
		$to = $sup_item[1];
	} else {
		$to = "";
	}
}

// Reply to and from of email
$reply = $to;
$from = "SchindlerMQI@gmail.com";
// echo "<html>";
// echo $from;
$to = "huuvan.tran@vn.schindler.com";
$cc = "huuvan20@gmail.com";
$reply = $to;
// echo $reply;
// echo "</html>"

// Send emails
// send_mail($from, $to, $cc, $reply, $email_title, $email_body);

// -------------Direct to dashboard page---------------
header("Location:user_dashboard.php");

?>