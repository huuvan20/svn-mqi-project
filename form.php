﻿<?php

require_once("inc/config.php");
require("items/items.php");
require("inc/functions.php");

include("inc/session_timeout.php");

if ( loggedin() ) {

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MQI Form | Schindler VN</title>
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" href="css/normalize.css">
 	<link href='http://fonts.googleapis.com/css?family=Changa+One|Open+Sans:400,400italic,700,700italic,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/tabs.css">
	<link rel="stylesheet" href="css/accordion.css">	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="css/main.css">

</head>

<body>
	<!-- HEADER AND MENU NAVIGATION -->
<header>
	<a href="index.php" id="logo">
		<h1>MAINTENANCE QUALITY INSPECTION</h1>
		<h2>Schindler VN</h2>
  	</a>
	<nav>
		<ul>
		  <li><a href="index.php">Home</a></li>
		  <li><a href="form.php" class="selected">Điền MQI</a></li>
		  <li><a href="monitors.php">Các findings</a></li>
		  <li><a href="MQI_results.php">Kết quả</a></li>
		</ul>
	</nav>
</header>
<div id="form-wrapper">

<form method="POST" action="form_to_database.php">

<div class="tabs">
	<ul class="tab-links">
		<li class="active"><a href="#tab1">Kiểm tra MQI</a></li>
		<li><a href="#tab2">Cách tính điểm</a></li>
		<li><a href="#tab3">Hướng dẫn sử dụng</a></li>
	</ul>

	<div class="tab-content">

<!-- ++++++++++++++++++++++++ Start form ++++++++++++++++++++++++++ -->

	<div id="tab1" class="tab active">
		
		<div class="whole-form">

			<div class="siteinfo-form">

				<h3><span class="number">1</span>Thông Tin Công Trình</h3>

				<!-- SITE NAME -->
				<label for="site_name">Tên công trình</label>
				<input name="site_name" id="site_name" value="<?php 
					if ( isset($_POST["site_name"]) && !empty($_POST["site_name"]) ) {
						echo $_POST["site_name"];
					}?>" required>
				
				<!-- INSPECTION DATE -->
				<label for="form_date">Ngày kiểm tra</label>
				<input type="text" id="form_date" name="date_check" value="<?php 
					if ( isset($_POST["date_check"]) && !empty($_POST["date_check"]) ) {
						echo $_POST["date_check"];
					}?>" required>

				<!-- LIFT NAME -->
				<label for="lift">Tên thang</label>
				<input name="lift" id="lift" value="<?php 
					if ( isset($_POST["lift"]) && !empty($_POST["lift"]) ) {
						echo $_POST["lift"];
					}?>" required>

				<!-- MRL OR MR -->
				<label for="MR_MRL">MR/MRL</label>
				<select id="MR_MRL" class="" onchange="MR_MRL_function()">
						<option value="MR">MR</option>
						<option value="MRL">MRL</option>
				</select>
				<script>
					function MR_MRL_function() {
						var x = document.getElementById("MR_MRL").value;
						var rankChange = document.getElementsByClassName("rank-MRL-1");
						if ( x === 'MRL' ) { 	
							for ( var counter=0; counter < rankChange.length; counter++ ) {
								rankChange[counter].value = 0;
							}
						} else if ( x === 'MR' ) {	
							for ( var counter=0; counter < rankChange.length; counter++ ) {
								rankChange[counter].value = 1;
							}
						}
					}
				</script>

				<!-- SUPERVISOR NAME -->
				<label for="sup">Giám sát</label>
				<select name="sup" id="sup" required>
				<?php foreach ($sups_list as $sup) { ?>
				<option value="<?php echo $sup[0]; ?>"
					<?php 
						if ( isset($_POST["sup"]) && !empty($_POST["sup"]) ) {
							if ( $_POST["sup"] == $sup[0] ) {
								echo " selected=\"selected\" ";
							}
						}
					?>
				><?php echo $sup[0]; ?></option>
				<?php } ?>
				</select>

				<!-- EI TECHNICIAN -->
				<label for="EI_tech_1">Nhân viên EI</label>
				<input name="EI_tech_1" id="EI_tech_1" value="<?php 
					if ( isset($_POST["EI_tech_1"]) && !empty($_POST["EI_tech_1"]) ) {
						echo $_POST["EI_tech_1"];
					}?>" required>
			
				<!-- RE-INSPECTION -->
				<label for="reinspection">Đã kiểm tra</label>
				<select name="reinspection" id="reinspection">
					<option value=1
						<?php
							if ( isset($_POST["reinspection"]) && !empty($_POST["reinspection"]) ) {
								if ( $_POST["reinspection"] == 1 ) {
									echo " selected=\"selected\" ";
								}
							}
						?>
					>Không</option>
					<option value=2
						<?php
							if ( isset($_POST["reinspection"]) && !empty($_POST["reinspection"]) ) {
								if ( $_POST["reinspection"] == 2 ) {
									echo " selected=\"selected\" ";
								}
							}
						?>
					>Có</option>
				</select>

				<!-- EQUIPMENT NUMBER -->
				<label for="equip_no">Số thiết bị</label>
				<input name="equip_no" id="equip_no" value="<?php 
					if ( isset($_POST["equip_no"]) && !empty($_POST["equip_no"]) ) {
						echo $_POST["equip_no"];
					}?>" class="" required>

				<!-- MWC NUMBER -->
				<label for="MWC">Số MWC</label>
				<select name="MWC" id="MWC" class="" required>
					<option value=""></option>
					<?php foreach ($MWC_list as $MWC) { ?>
					<option value="<?php echo $MWC; ?>"
						<?php 
							if ( isset($_POST["MWC"]) && !empty($_POST["MWC"]) ) {
								if ( $_POST["MWC"] == $MWC ) {
									echo " selected=\"selected\" ";
								}
							}
						?>
					><?php echo $MWC; ?></option>
					<?php } ?>
				</select>
				
				<!-- SALE OFFICE -->
				<label for="sales_office">Số Sales Office</label>
				<select name="sales_office" id="sales_office" class="" required>
					<option value=""></option>
					<?php foreach ($sales_office_list as $sale_office) { ?>
					<option value="<?php echo $sale_office[0]; ?>"
						<?php 
							if ( isset($_POST["sales_office"]) && !empty($_POST["sales_office"]) ) {
								if ( $_POST["sales_office"] == $sale_office[0] ) {
									echo " selected=\"selected\" ";
								}
							}
						?>
					><?php echo $sale_office[0]; ?></option>
					<?php } ?>
				</select>
			
			</div>


<!--+++++++++++++++++++++ CONSTRUCTING THE MQI CHECKLIST FORM +++++++++++++++++++++++-->

			<div class="checklist-form">
				<h3><span class="number">2</span>Các Mục Kiểm Tra</h3>
				<div class="tab-group">
					<?php 
					$i = 0;
					$j = 0;
					foreach ($items_list as $items_list_section) {
						$j++;
					?>
					<div class="tab-c">
						<input id="tab-<?php echo $j; ?>" type="checkbox" name="tabs">
						<label for="tab-<?php echo $j; ?>"><?php echo $titles_list[$j-1]; ?></label>
						<div class="tab-content-c">
							<div class="table-form">

							<?php
								foreach ($items_list_section as $item) {
									$i++;
							?>
								
								<div class="check-point-details">
									
									<div>
									<legend class="index-form-<?php echo $i; ?>">
										<?php echo $item[0]; ?>
									</legend>

									<label  class="item-form-<?php echo $i; ?>">
										<?php echo $item[1]; ?>
									</label>
									</div>

									<div>
									<label for="rank[<?php echo $i; ?>]">Đánh giá</label>
									<select name="rank[<?php echo $i; ?>]" id="rank[<?php echo $i; ?>]" class="rank-form-<?php echo $i; ?> rank-MRL-<?php echo $j; ?>">
										<?php foreach ($rank_list as $rank) { ?>
										<option value=<?php echo $rank; ?> 
										<?php 
											if ($rank==1) {
												echo "selected";
											}
										?>
										>
											<?php 
												if ( $rank==0 ) {
													echo "N/A";
												} else {
													echo $rank;
												}
											?>
										</option>
										<?php } ?>
									</select>
									</div>
									<div>
									<label class="weight-form-label"
									<?php 
										if ($item[2]==1) {
											echo "for=\"weight-form-1\"";
										} elseif ($item[2]==2) {
											echo "for=\"weight-form-2\"";
										} elseif ($item[2]==3) {
											echo "for=\"weight-form-3\"";
										}
									?>>Trọng số</label>
									<p class="weight-<?php echo $i;
										if ($item[2]==1) {
											echo " weight-form-1";
										} elseif ($item[2]==2) {
											echo " weight-form-2";
										} elseif ($item[2]==3) {
											echo " weight-form-3";
										}
									?>">
										<?php echo $item[2]; ?>
									</p>
									</div>

									<div>
									<label for="description[<?php echo $i; ?>]">Mô tả lỗi</label>
									<textarea name="description[<?php echo $i; ?>]" id="description[<?php echo $i; ?>]" class="des-form-<?php echo $i; ?>"><?php 
											if ( isset($_POST["description"][$i]) && !empty($_POST["description"][$i]) ) {
													echo $_POST["description"][$i];
												}
									?></textarea>
									</div>									
									<div>
									<label for="remark[<?php echo $i; ?>]">Ghi chú</label>
									<select name="remark[<?php echo $i; ?>]" id="remark[<?php echo $i; ?>]" class="remark-form-<?php echo $i; ?>">
										<?php foreach ($remarks_list as $remark) { ?>
										<option value="<?php echo $remark; ?>"
											<?php 
												if ( isset($_POST["remark"][$i]) && !empty($_POST["remark"][$i]) ) {
													if ( $_POST["remark"][$i] == $remark ) {
														echo " selected=\"selected\" ";
													}
												}
											?>
										><?php echo $remark; ?></option>
										<?php } ?>
									</select>
									</div>

									<div>
									<label for="status[<?php echo $i; ?>]">Tình trạng</label>
									<select name="status[<?php echo $i; ?>]" id="status[<?php echo $i; ?>]" class="status-form-<?php echo $i; ?>">
										<?php foreach ($status_list as $status) { ?>
										<option value="<?php echo $status; ?>" 
											<?php 
												if ( isset($_POST["status"][$i]) && !empty($_POST["status"][$i]) ) {
													if ( $_POST["status"][$i] == $status ) {
														echo " selected=\"selected\" ";
													}
												}
											?>
										><?php echo $status; ?></option>
										<?php } ?>
									</select>
									</div>

									<div>
									<label for="responsibility[<?php echo $i; ?>]">Trách nhiệm</label>
									<select name="responsibility[<?php echo $i; ?>]" id="responsibility[<?php echo $i; ?>]" class="res-form-<?php echo $i; ?>">
										<?php foreach ($responsibility_list as $responsibility) { ?>
										<option value=<?php echo $responsibility; ?> 
											<?php 
												if ( isset($_POST["responsibility"][$i]) && !empty($_POST["responsibility"][$i]) ) {
													if ( $_POST["responsibility"][$i] == $responsibility ) {
														echo " selected=\"selected\" ";
													}
												}
											?>
										>
											<?php 
											if ($responsibility == 2) { 
												echo "Có";
											} elseif ($responsibility == 1) { 
												echo "Ko"; 
											} 
											?>
										</option>
										<?php } ?>
									</select>
									</div>
								</div>
							
							<?php } ?>

							</div>

						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			<div id="overlay">
			<img src="img/close.png">
			<div class="form-in-review">
				<div class="review-site-info">
					<div class="review-site-name">
						<label>Tên công trình</label>
						<p></p>
					</div>
					<div class="review-date-check">
						<label>Ngày kiểm tra</label>
						<p></p>
					</div>
					<div class="review-lift-name">
						<label>Thang</label>
						<p></p>
					</div>
					<div class="review-MR-type">
						<label>Loại phòng máy</label>
						<p></p>
					</div>
					<div class="review-sup-name">
						<label>Giám sát</label>
						<p></p>
					</div>
					<div class="review-EI-technician">
						<label>Nhân viên bảo trì</label>
						<p></p>
					</div>
					<div class="review-checked">
						<label>Đã kiểm tra</label>
						<p></p>
					</div>
					<div class="review-unit-number">
						<label>Số thiết bị</label>
						<p></p>
					</div>
					<div class="review-MWC">
						<label>Số MWC</label>
						<p></p>
					</div>
					<div class="review-sale-office">
						<label>Số sale office</label>
						<p></p>
					</div>
				</div>
				<div class="review-scores">
					<div class="review-fitter-score">
						<label>Điểm nhân viên EI</label>
						<p class="score"></p>
						<P class="scorePer"></p>
					</div>
					<div class="review-site-score">
						<label>Điểm công trình</label>
						<p class="score"></p>
						<p class="scorePer"></p>
					</div>
					<div class="review-total-score">
						<label>Điểm tổng</label>
						<p class="score"></p>
						<p class="scorePer"></p>
					</div>
					<div class="review-result">
						<label>Kết luận</label>
						<p class="score"></p>
						<p class="scorePer"></p>
					</div>
				</div>
				<div class="NCs-container">
				</div>
			</div>
			</div>
		<!-- Show the review & submit button in case of MQI auditors -->
		<?php 
			$mqi_users_list = get_role_users( 'mqi' );
			if ( in_array($_SESSION["user_name"], $mqi_users_list) ) {
		?>
		<input type="button"  class="button button-review" value="Xem Trước Tóm Tắt">
		<input type="submit"  class="button button-submit" value="Nộp Báo Cáo">
		<?php } ?>
		</div>
	</div>
<!-- ANOTHER TABS -->
		<div id="tab3" class="tab">
			<ul>
				<li>Phần "Thông tin công trình" cần được điền đầy đủ tất cả các mục.</li>
				<li>Mặc định là nhân viên bảo trì đã hoàn thành tốt tất cả các mục. Còn nếu có lỗi ở mục nào thì bạn điền vào lỗi tương ứng của mục đó.</li>
				<li>Sau khi hoàn tất, nhấn nút "Nộp báo cáo". Để xem lại các lỗi tìm thấy trong kiểm tra MQI hay kết quả công trình (điểm số, đậu, rớt), bạn có thể truy cập mục Các findings hoặc Kết quả ở trên menu</li>			
			</ul>
		</div>
		<div id="tab2" class="tab">
			<?php include 'form_instructions.php'; ?>
		</div>
	</div>
</div>
</form> 
<footer>
	<p><a href="logout.php">Đăng xuất</a></p>
	<p>&copy; 2015 Schindler VN</p>
</footer>
</div>
	<script src="http://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript" charset="utf-8"></script>
	<!-- Datepicker plugin-->
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
	<!-- forms -->
	<script src="js/form.js" type="text/javascript" charset="utf-8"></script>
	<!-- TABS plugin -->
	<script src="js/tabs.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>
<?php
} else {
	header("Location:index.php");
} 
?>