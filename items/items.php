<?php

$items_list = array (//sections, items and weight respectively
array(
			array("A.1.1","Lối vào phòng máy, lan can và đèn chiếu sáng",1),
			array("A.1.2","Cửa phòng máy có khóa và nhãn cảnh báo nguy hiểm",1),
			array("A.1.3","Có đủ hệ thống thông gió và chiếu sáng",1),	
			array("A.1.4","Tủ nguồn điện chính của tòa nhà được bảo vệ và dễ nhận thấy",1),	
			array("A.1.5","Có nhãn cảnh báo điện áp cao trên cầu dao nguồn điện",1),	
			array("A.1.6","Phòng máy giữ sạch sẽ, gọn gàng, nếu có khu vực để đồ thì phải được sơn vạch vàng",1),	
			),

array(
			array("A.2.1","Sạch sẽ, không bị rò rỉ dầu nhớt, không có tiếng ồn lạ, mực nhớt (đối với có hộp số)",2),	
			array("A.2.2","Tình trạng quạt giải nhiệt và bộ lọc bụi",2),	
			array("A.2.3","Có cần cảo thắng và được đặt đúng nơi quy định",3),	
			array("A.2.4","Có bảng hướng dẫn cứu hộ bằng tiếng việt",2),	
			array("A.2.5","Đánh dấu trên cáp vị trí bằng tầng",1),				
			array("A.2.6","Tình trạng của puli, bát chống nhảy cáp",2),
			array("A.2.7","Khe hở thắng",3),	
			array("A.2.8","Trống thắng, đĩa thắng không bị vênh, bụi bẩn và dầu mỡ",3),
			),

array(
			array("A.3.1","Cửa tủ điều khiển có khóa và ở điều kiện tốt",1),	
			array("A.3.2","Sạch sê không có bụi, hơi ẩm, dây điện gọn gàng",2),	
			array("A.3.4","Sổ nhật ký hư hỏng và sơ đồ mạch điện",1),	
			array("A.3.5","Tình trạng bình ắc quy",3),	
			array("A.3.6","Chức năng của intercom (thang MMR và MRL)",3),
			array("A.3.7","Có bảng hướng dẫn cứu hộ bằng tiếng việt (MRL)",2),				
			array("A.3.8","Chức năng bộ cảo thắng bằng tay (MRL) / PEBO",3),
			array("A.3.9","Chức năng của EBOPS (MRL và MMR)",2),	
			array("A.3.10","Chức năng bộ cứu hộ tự động ARD/ARS, tình trạng bình ắc quy",2),	
			array("A.3.11","Chức năng của quạt làm mát các tủ điện, VF",2),
			),

array(
			array("A.4.1","Độ căng cáp, vị trí cố định cáp, các chốt chẻ, con tán, chống xoắn cáp và kẹp cáp",3),	
			array("A.4.2","Tình trạng của cáp (hao mòn, gỉ sét và hư hỏng), tình trạng bôi trơn cáp (tùy theo loại cáp)",3),
			),

array(
			array("A.5.1","Chiều quay, nhãn thông số, nêm chì, sự cố định, bảo vệ cáp, tình trạng cáp",3),	
			),

array(
			array("A.6.1","Khoá cửa tầng",2),	
			array("A.6.2","Cân chỉnh (chức năng, tự khóa, đóng mở êm)",3),
			),

array(
			array("A.7.1","Sạch sẽ",1),				
			array("A.7.2","Kiểm tra các công tắc, chức năng và tình trạng của các nút STOP, đèn nóc cabin",3),
			array("A.7.3","Lan can, hộp nhớt, tình trạng của các thiết bị: OKR, thiết bị dò dọc hố, nắp thoát hiểm, quạt làm mát",2),	
			array("A.7.4","Cần giật thắng cơ có 2 chốt ống, bulong chặn, cần đẩy, con tán khóa, khớp nối của cáp Governor",3),	
			array("A.7.5","Bộ khóa giữ cabin cho thang MRL (CBD)",2),
			),

array(
			array("A.8.1","Sạch sẽ, có đầy đủ ánh sáng, kiểm tra khoảng cách giữa buffer và đối trọng",3),	
			array("A.8.2","Vị trí cố định cáp travelling và nắp chụp máng điện",2),	
			array("A.8.3","Bát treo của bộ khóa giữ cabin (CBD bracket)",2),	
			array("A.8.4","Chức năng các nút STOP",3),
			array("A.8.5","Lối vào hố thang, thang mèo, đèn trong hố, tấm che đối trọng",2),				
			array("A.8.6","Buffer được cố định chắc chắn, mức dầu, thiết bị căng cáp Governor",3),
			),

array(
			array("A.9.1","Gắn cố định xích bù, dẫn hướng xích bù, quấn cáp an toàn",3),	
			array("A.9.2","Bộ chống nhảy cáp bù ASS, tiếp điểm",3),	
			),

array(
			array("A.10.1","Thắng cơ (sạch sẽ, có nêm chì, chức năng hoạt động, má thắng, chốt lò xo, các chi tiết di chuyển tốt)",3),	
			array("A.10.2","Bánh xe dẫn hướng, shoe trượt dẫn hướng, bát chống dẫn hướng an toàn (emergency guide)",3),
			),

array(
			array("A.11.1","Cân chỉnh, quá trình hoạt động",1),	
			array("A.11.2","Thiết bị an toàn cửa (nút mở cửa, công tắc giới hạn, photocell)",2),
			),

array(
			array("A.12.1","Trần cabin sạch sẽ, có dây đai an toàn",2),	
			array("A.12.2","Đèn chiếu sáng, quạt làm mát, tay vịn",2),	
			array("A.12.3","Điện thoại liên lạc, nút khẩn cấp, hiển thị tầng, nút gọi",2),	
			array("A.12.4","Độ bằng tầng, thang chạy êm",2),	
			),

array(
			array("A.13.1","Hố thang phải sạch sẽ (bộ truyền động, các thiết bị căng xích)",2),	
			array("A.13.2","Nhãn cảnh báo dán trên kính ở 2 đầu",2),	
			array("A.13.3","Tình trạng của tấm răng lược, bậc thang, chức năng các nút STOP và đầu vào của tay vịn",3),	
			array("A.13.4","Tình trạng và độ êm của thang khi hoạt động",2),
			),

array(
			array("B.1.1","Thông báo cho chủ tòa nhà khi bắt đầu, kết thúc công việc, có biển cảnh báo tại khu vực làm việc",1),	
			array("B.1.2","Đồ bảo hộ còn tốt, sử dụng đúng",2),	
			array("B.1.3","Có rào chắn ở nơi làm việc",2),	
			array("B.1.4","Giao tiếp lịch sự, không hút thuốc ở nơi làm việc",1),
			array("B.1.5","Đồ nghề còn tốt và bảo quản tốt, có và biết cách sử dụng LOTO, cục chặn cửa…",2),	
			array("B.1.6","Tuân thủ quy trình bảo trì Expert Plus",2),
			),

array(
			array("C.1.1","Các FI, DLFI đã xử lý xong đúng như đã báo cáo",3),
			array("C.1.2","NC đã xử lý xong đúng như đã báo cáo",2),	
			array("C.1.3","RAM: các công việc đặc thù yêu cầu riêng cho công trình",2),
			)
	
);

$titles_list = array(

	"A. Maintenance Quality / Chất lượng bảo trì - 1. Machine Room / Phòng máy",
	"2. Machine & Brake / Máy kéo và thắng",
	"3. Controller / Tủ điều khiển",
	"4. Suspension Media / Hệ thống treo cáp tải của cabin và đối trọng",
	"5. Governor / Bộ khống chế vượt tốc Governor",
	"6. Landing Doors / Cửa tầng",
	"7. On Car Top / Nóc cabin",
	"8. Hoistway & Pit / Hố thang và đáy hố",
	"9. Conpensation Media / Thiết bị bù",
	"10. CAR & CWT Components / Các chi tiết của cabin và đối trọng",
	"11. Car Door Drive / Thiết bị truyền động cửa cabin",
	"12. Car Inside / Bên trong cabin",
	"13. Escalator / Thang cuốn",
	"B. Safety Behaviour & Attitude / Nhận thức về an toàn và thái độ khi làm việc",
	"C. FI, DLFI, NC, RAM"

	);


$sups_list = array(
	array("",""),
	array("Nguyen Dang Hai","hai.nguyen@vn.schindler.com"),
	array("Tran The Ky - Team 300","trantheky79@gmail.com"),
	array("Nguyen Canh Duc","duc.nguyen@vn.schindler.com"),
	array("Ho Van Hoa","hohoa54@yahoo.com.sg"),
	array("Doan Van Tan",""),
	array("Vo Xuan Cuong","cuong.vo@vn.schindler.com"),
	array("Tran Van Thong","thong.tran@vn.schindler.com"),
	array("Huynh The Binh","binh.huynh@vn.schindler.com"),
	array("Dieu Ngoc Dieu",""),
	array("Duong Minh Dung","dung.duong@vn.schindler.com"),
	array("Nguyen Trung Hieu","hieu.trung.nguyen@vn.schindler.com"),
	array("Nguyen Thanh Sang","sang.nguyen@vn.schindler.com"),
	array("Tran Anh Duc","trananhduc1309@gmail.com"),
	array("Nguyen Chi Hieu","hieu.nguyen@vn.schindler.com"),
	array("Le Minh Quan",""),
	array("Nguyen Ngoc Son",""),
	array("Trung - Vy","Ngan.Dau@vn.schindler.com"),
	array("Vu Huy Cong",""),
	array("Cung Khac Nghi",""),
	array("Ta Minh Dung",""),
	array("Nghiem Van Dong",""),
	array("Nguyen Tien Dat",""),
	array("Tran Ha Lam",""),
	array("Vu Quang Huy","")	
	);

$sales_office_list = array(
	array(6501,"tung.nguyen@vn.schindler.com"),
	array(6502,"dung.cao@vn.schindler.com"),
	array(6503,"luong.le@vn.schindler.com"),
	array(6507,"ngan.dau@vn.schindler.com"),
	array(6511,"anh.ha@vn.schindler.com"),
	array(6512,"son.vu@vn.schindler.com")
	);

$MWC_list = array(
		650111,
		650112,
		650113,
		650114,
		650115,
		650116,
		650121,
		650122,
		650123,
		650124,
		650125,
		650126,
		650131,
		650132,
		650133,
		650134,
		650141,
		650142,
		650143,
		650211,
		650212,
		650213,
		650214,
		650221,
		650222,
		650223,
		650224,
		650231,
		650232,
		650233,
		650234,
		650241,
		650242,
		650243,
		650244,
		650245,
		650311,
		650312,
		650313,
		650314,
		650321,
		650322,
		650323,
		650324,
		650325,
		650331,
		650332,
		650333,
		650334,
		650341,
		650342,
		650343,
		650344,
		650731,
		650732,
		650711,
		650712,
		650713,
		650721,
		650722,
		650723,
		650724,
		650725,
		650251,
		650252,
		650253,
		650254,
		650261,
		650351,
		650352,
		650353,
		650354,
		651111,
		651112,
		651113,
		651114,
		651115,
		651121,
		651122,
		651123,
		651124,
		651125,
		651126,
		651131,
		651132,
		651133,
		651134,
		651135,
		651211,
		651212,
		651213,
		651214,
		651221,
		651222,
		651223,
		651224,
		651225,
		651231,
		651232,
		651233,
		651234,
		651235,
		651236,
		651237,
		651238,
		651411
		);

$status_list = array(
	"",
	"Done",
	"Noted as impr. point",
	"Quoted",
	"To be followed up",
	"Waiting for material",
	"Reminded",
	"Letter to customer",
	"Shutdown"
	);

$remarks_list = array(
	"",
	"Safety Improvement",
	"Immediate Risk NC",
	"High Potent. Risk NC",
	"Low Potent. Risk NC",
	"Maintenance Quality",
	"Behaviour & Attitude",
	"Corrective Action"
	);

// Criteria for critical item
$critical_item = "Immediate Risk NC";

$show_amount_list = array(
	100,
	200,
	500,
	""
	);

$rank_list = array(
	-3,
	-2,
	-1,
	1,
	1.5,
	0
	);

$responsibility_list = array(
	2,
	1
	);

$user_role_list = array(
	"sup",
	"mqi",
	"other",
	"admin"
	);

?>

