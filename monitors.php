<?php 

require_once("inc/config.php");
require("inc/functions.php");
require("items/items.php");

include("inc/session_timeout.php");

if ( loggedin() ) {

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <title>MQI Findings | Schindler VN</title>
	    <meta name="viewport" content="width=device-width">
		<link rel="stylesheet" href="css/normalize.css">
	    <link href='http://fonts.googleapis.com/css?family=Changa+One|Open+Sans:400,400italic,700,700italic,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
		<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="css/main.css">

	</head>
	<body>
		<header>
		<a href="index.php" id="logo">
	    	<h1>MAINTENANCE QUALITY INSPECTION</h1>
	    	<h2>Schindler VN</h2>
		</a>
     	<nav>
	        <ul>
	          <li><a href="index.php">Home</a></li>
	          <li><a href="form.php">Điền MQI</a></li>
	          <li><a href="monitors.php" class="selected">Các findings</a></li>
	          <li><a href="MQI_results.php">Kết quả</a></li>
	        </ul>
    	</nav>
		</header>
		<div id="wrapper-findings">

			<?php
			if(isset($_POST["action"]) && $_POST["action"] == "update") {

				if (isset($_POST["status"])) {
					foreach ($_POST["status"] as $id_key => $value) { 					//get the id of finding
						
						foreach ($value as $user_key => $status_value) {				//get the $user_table and status value

							$current_status = get_finding_status($id_key, $user_key);	// Get the current status
							$current_record = get_finding_record($id_key, $user_key);	// Get the current record
							
							if ($current_status != $status_value){						// just for status changes

								date_default_timezone_set('Asia/Bangkok');
								$date = date('Y-m-d h:i:s');

								$new_record = $_SESSION["user_name"]." change from ".$current_status." to ".$status_value." on ".$date;// record user's changes

								update_finding_status($id_key, $user_key, $status_value, $current_record, $new_record);

							}

							if ( $status_value == 'Done' ) {

								date_default_timezone_set('Asia/Bangkok');
								$date_close = date('Y-m-d');

								update_date_close($user_key, $id_key, $date_close);

							}

						}
					}
				}
			}

			?>



<!--+++++++++++++++++++++++++++ FORM FOR SEARCHING ++++++++++++++++++++++++++++++++-->

	<form action="<?php echo $current_file; ?>" method="post" class="monitors-form">

		<div class="monitors-div-search">
			<ul class="panel-group">

				<!-- SALES OFFICE SEARCH -->
		
	            <li class="panel-body">
	                <select id="sales_office_search" name="sales_office_search[]" multiple="multiple" >
						<?php 
							foreach ($sales_office_list as $sales_office) { 
						?>
						<option value="<?php echo $sales_office[0]; ?>"
							<?php
								if ( isset($_POST["sales_office_search"]) && !empty($_POST["sales_office_search"]) ) {
									if ( in_array($sales_office[0], $_POST["sales_office_search"]) ) {
										echo " selected=\"selected\" ";
									}
								}
							?>
						><?php echo $sales_office[0]; ?></option>
						<?php } ?>
					</select>
	            </li>

	            <!-- MWC SEARCH -->
		
	            <li class="panel-body">
	                <select id="MWC_search" name="MWC_search[]" multiple="multiple" >
						<?php 
							foreach ($MWC_list as $MWC) { 
						?>
						<option value="<?php echo $MWC; ?>"
							<?php
								if ( isset($_POST["MWC_search"]) && !empty($_POST["MWC_search"]) ) {
									if ( in_array($MWC, $_POST["MWC_search"]) ) {
										echo " selected=\"selected\" ";
									}
								}
							?>
						><?php echo $MWC; ?></option>
						<?php } ?>
					</select>
	            </li>
		   		
		   		<!-- STATUS SEARCH -->
			
	            <li class="panel-body">
	                <select id="status_search" name="status_search[]" multiple="multiple" >
						<?php 
							foreach ($status_list as $status) { 
						?>
						<option value="<?php echo $status; ?>"
							<?php
								if ( isset($_POST["status_search"]) && !empty($_POST["status_search"]) ) {
									if ( in_array($status, $_POST["status_search"]) ) {
										echo " selected=\"selected\" ";
									}
								}
							?>
						>
							<?php 
							if (empty($status)){
								echo 'None';
							} else {
								echo $status;
							}
							?>
						</option>
						<?php } ?>
					</select>
	            </li>

	            <!-- REMARKS SEARCH -->
	            
	            <li class="panel-body">
	                <select id="remark_search" name="remark_search[]" multiple="multiple" >
						<?php 
							foreach ($remarks_list as $remark) { 
						?>
						<option value="<?php echo $remark; ?>"
							<?php
								if ( isset($_POST["remark_search"]) && !empty($_POST["remark_search"]) ) {
									if ( in_array($remark, $_POST["remark_search"]) ) {
										echo " selected=\"selected\" ";
									}
								}
							?>
						>
							<?php 
							if (empty($remark)){
								echo 'None';
							} else {
								echo $remark;
							}
							?>
						</option>
						<?php } ?>
					</select>
	            </li>
	       		
	       		<!-- DATE FROM SEARCH -->
	            <li class="panel-body">
	                <input type="text" id="from" name="date_from" value="<?php
	                	if ( isset($_POST["date_from"]) ) {
	                		echo $_POST["date_from"];
	                	}
	                 ?>" class="panel-body-date" placeholder="From">
	            </li>
				
	            <!-- DATE TO SEARCH -->
	            <li class="panel-body">
	                <input type="text" id="to" name="date_to" value="<?php
	                	if ( isset($_POST["date_to"]) ) {
	                		echo $_POST["date_to"];
	                	}
	                 ?>" class="panel-body-date" placeholder="To">
	            </li>
	        	
	        	<!-- AMOUNT SEARCH -->
		    	<li class="panel-body">
		    		<select name="show_amount" class="panel-body-date show-amount-class">
		    			<?php foreach ($show_amount_list as $show_amount) { ?>
		    			<option value=<?php echo $show_amount; ?> 
		    				<?php
								if ( isset($_POST["show_amount"]) && !empty($_POST["show_amount"]) ) {
									if ( $_POST["show_amount"] == $show_amount ) {
										echo " selected=\"selected\" ";
									}
								}
							?>
		    			>
		    				<?php 
		    				if ( empty($show_amount) ) {
		    					echo "ALL"; 
		    				} else {
		    					echo $show_amount;
		    				}
		    				?>
		    			</option>
		    			<?php } ?>
		    		</select>
	    		</li>
			</ul>
		</div>
				
		<input type="hidden" name="search" value="update">
		<input type="submit" value="Tìm kiếm" class="button button-monitor">
	</form>


<!-- +++++++++++++++++++ FORM FOR FINDINGS DETAILS ++++++++++++++++++++++ -->

<?php if(isset($_POST["search"]) && $_POST["search"] == "update") { ?>

	<form action="<?php echo $current_file; ?>" method="post">
		<table class="monitors-results">
			<tr>
				<th>Insp. date</th>
				<th>MWC</th>
				<th>S.O.</th>
				<th>Technician</th>
				<th>Project/ Site</th>
				<th>Equip. no.</th>
				<th>Lift</th>
				<th>Items</th>
				<th>Descriptions</th>
				<th>Remarks</th>
				<th>Status</th>
				<th>Days</th>
			</tr>
<?php 
	$sales_office_search 	= "";
	$MWC_search 			= "";
	$status_search 	= "";
	$remark_search = "";
	$date_from 	= "";
	$date_to 	= "";

	if(isset($_POST["search"]) && $_POST["search"] == "update") {

		if ( isset($_POST["sales_office_search"]) ) { $sales_office_search = $_POST["sales_office_search"];}
		if ( isset($_POST["MWC_search"]) ) { $MWC_search	= $_POST["MWC_search"];}
		if ( isset($_POST["status_search"]) ) { $status_search = $_POST["status_search"];}
		if ( isset($_POST["remark_search"]) ) { $remark_search = $_POST["remark_search"];}

		if ( isset($_POST["date_from"]) && !empty($_POST["date_from"]) ) { 
			$date_from = $_POST["date_from"];
			$date_from = date('Y-m-d', strtotime($date_from));
		}
		if ( isset($_POST["date_to"]) && !empty($_POST["date_to"]) ) { 
			$date_to = $_POST["date_to"];
			$date_to = date('Y-m-d', strtotime($date_to));
		}

		$show_amount 	= $_POST["show_amount"];

		$mqi_users_list = get_role_users( 'mqi' );

		date_default_timezone_set('Asia/Bangkok');
		$date_close = date('Y-m-d');

		$i = 0;

		foreach ($mqi_users_list as $user) {

			$findings_list = get_monitors_info($user, $sales_office_search, $MWC_search, $status_search, $remark_search, $date_from, $date_to, $show_amount);

			foreach ($findings_list as $finding_elem) { 

				$findings_array[$i] = $finding_elem;
				$i++;

			}
		}

			// Sort the array values by dates
		if ( !empty($findings_array) ) {

			usort($findings_array, 'date_compare');

			$j = 0;

			foreach ($findings_array as $finding) {
				$j++;
				if ( $j <= $show_amount || empty($show_amount) ) {

?>
						<tr>
							<td><?php 
								$date_check = $finding["date_check"];
								$new_form_date = date("d/m/y", strtotime($date_check));
								echo $new_form_date; 
							?></td>
							<td class="monitors-highlight"><?php echo $finding["MWC"]; ?></td>
							<td class="monitors-highlight"><?php echo $finding["sales_office"]; ?></td>
							<td><?php echo $finding["EI_tech_1"]; ?></td>
							<td><?php echo $finding["site_name"]; ?></td>
							<td><?php echo $finding["equip_no"]; ?></td>
							<td><?php echo $finding["lift"]; ?></td>
							<td><?php echo $finding["index"]; ?></td>
							<td><?php echo $finding["descriptions"]; ?></td>
							<td><?php echo $finding["remarks"]; ?></td>

							<td class="monitors-highlight">

								<?php
									if ( $finding["status"] == "Done" ) {

										echo $finding["status"];

									} else {
										$id 			= $finding["id"];
										$username 		= $finding["username"];
										$finding_status = $finding["status"];

										echo "<select name=\"status[$id][$username]\">";

										foreach ($status_list as $status) { 
											if ($status != $finding_status) {
												echo "<option value=\"$status\">$status</option>";
											}
										}
										
										echo "<option value=\"$finding_status\" selected=\"selected\">$finding_status</option>";	
										echo "</select>";
									}
								?>

							</td>

							<?php 
								if ( !empty($finding["date_close"]) || $finding["status"] == "Done" ) {
									echo "<td class=\"green-days\">";
								} else {
									$days_gap = days_gap_inspec_to_close($finding["id"], $finding["username"], $date_close);
									if ($days_gap >= 200) {
										echo "<td class=\"red-days\">";
									} elseif ($days_gap >= 100 && $days_gap < 200 ) {
										echo "<td class=\"orange-days\">";
									} elseif ($days_gap >= 50 && $days_gap < 100) {
										echo "<td class=\"yellow-days\">";
									} else {
										echo "<td>";
									}
								}
							 ?>
								<?php 
								if ( !empty($finding["date_close"]) || $finding["status"] == "Done" ) {
									echo $finding["date_close"];
								} else {
									echo $days_gap;
								}
								?>
							</td>
						</tr>

						<?php 
						} 
					} 
				} 
			}?>
		</table>
		<input type="hidden" name="action" value="update" />
		<input type="submit" value="Update status" class="button">
	</form>
<?php } ?>
			<footer>
				<p><a href="logout.php">Đăng xuất</a></p>
				<p>&copy; 2015 Schindler VN</p>
			</footer>
		</div>
	<!-- jQuery and Boostrap included -->
	<script src="http://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- Bootstrap multiselect jQuery -->
	<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
	<!-- UI JQUERY PLUGIN (DATEPICKER) -->
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<!-- MAIN JS -->
	<script type="text/javascript" src="js/main.js"></script>
	</body>
</html>

<?php 
	} else {
		header("Location:index.php");
	}
?>

