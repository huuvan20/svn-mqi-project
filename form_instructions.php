<div>

	<table class="table-how-status">
		<tr>
			<th class="english-term">INSP REPORT/Status</th>
			<th class="english-term"><p>Giải thích</p></th>
		</tr>
		<tr>
			<td class="english-term">DONE</td>
			<td><p>Hạng mục được khắc phục</p></td>
		</tr>
		<tr>
			<td class="english-term">Noted As Improvement Point</td>
			<td><p>Đề xuất cải thiện không bắt buộc phải có biện pháp khắc phục hiện tại</p></td>
		</tr>
		<tr>
			<td class="english-term">Quoted</td>
			<td><p>Đã gửi báo giá cho khác hàng</p></td>
		</tr>
		<tr>
			<td class="english-term">To Be Followed Up</td>
			<td><p>Hạng mục không thể hoàn thành ngay, sẽ được khắc phục sau và báo cáo</p></td>
		</tr>
		<tr>
			<td class="english-term">Waiting For Material</td>
			<td><p>Vật tư không có sẵn, đã đặt hàng, chờ hàng về</p></td>
		</tr>
		<tr>
			<td class="english-term">Bỏ trống</td>
			<td><p>Hạng mục để trống nghĩa là SM/SUP chưa xem lại lỗi và chưa có kế hoạch khắc phục</p></td>
		</tr>
	</table>

<table class="table-how-reference">
		<tr>
			<td class="english-term"></td>
			<th class="english-term">Weight</th>
			<th colspan="3" class="english-term">Low to High</th>
		</tr>
		<tr>
			<th class="english-term">Scale</th>
			<td class="how-number english-term"></td>
			<td class="how-green how-number">1</td>
			<td class="how-yellow how-number">2</td>
			<td class="how-red how-number">3</td>
		</tr>
		<tr>
			<th rowspan="5" class="english-term">Poor to Excellent</th>
			<td class="how-red how-number">-3</td>
			<td class="how-number">-3</td>
			<td class="how-number">-6</td>
			<td class="how-number">-9</td>
		</tr>
		<tr>
			<td class="how-orange how-number">-2</td>
			<td class="how-number">-2</td>
			<td class="how-number">-4</td>
			<td class="how-number">-6</td>
		</tr>
		<tr>
			<td class="how-yellow how-number">-1</td>
			<td class="how-number">-1</td>
			<td class="how-number">-2</td>
			<td class="how-number">-3</td>
		</tr>
		<tr>
			<td class="how-lightgreen how-number">1</td>
			<td class="how-number">1</td>
			<td class="how-number">1</td>
			<td class="how-number">1</td>
		</tr>
		<tr>
			<td class="how-green">1.5</td>
			<td class="how-number">1.5</td>
			<td class="how-number">1.5</td>
			<td class="how-number">1.5</td>
		</tr>
	</table>

	<table class="table-how-remark">
		<tr>
			<th class="english-term">INSP REPORT/Finding Remark</th>
			<th class="english-term">Giải thích</th>
			<th class="english-term">Điểm MQI</th>
			<th class="english-term">Scale</th>
			<th class="english-term">Weight</th>
		</tr>
		<tr>
			<td class="english-term">Safety Improvement</td>
			<td>
				<p>Góp ý cải thiện về an toàn</p>
				<p>Ví dụ trường hợp thang trước SAIS lan can và nóc cabin không đạt chuẩn, không có nút stop thứ ở đáy hố, thiếu bửng che đối trọng,...</p>
				<p>Lỗi được lưu lại nhưng không trừ điểm</p>
			</td>
			<td>1</td>
			<td>1</td>
			<td>1</td>
		</tr>
		<tr>
			<td class="english-term">Immediate Risk NC</td>
			<td>
				<p>Lỗi Immediate Risk NC, mức rủi ro từ 9-12</p>
				<p>Buộc phải dừng thang nếu không được khắc phục ngay. Để thang vào tình trạng này là nhân viên bảo trì đã không theo dõi, đánh giá đúng mức trong những lần bảo trì trước đây. Nên nhân viên và thang đều bị trừ điểm</p>
			</td>
			<td>-9</td>
			<td>-3</td>
			<td>3</td>
		</tr>
		<tr>
			<td class="english-term">High Potential Risk NC</td>
			<td><p>Mức rủi ro từ 5-8, NC phải được khắc phục trong 1 tháng</p></td>
			<td>Scale x Weight</td>
			<td>Variable</td>
			<td>Variable</td>
		</tr>
		<tr>
			<td class="english-term">Low Potential Risk NC</td>
			<td><p>Mức rủi ro 1-4, NC phải được khắc phục trong 3 tháng</p></td>
			<td>Scale x Weight</td>
			<td>Variable</td>
			<td>Variable</td>
		</tr>
		<tr>
			<td colspan="5" class="how-highlight"><p>Chi tiết về cách đánh giá tham khảo tài liệu CPSI J42500103</p></td>
		</tr>
		<tr>
			<td class="english-term">Maintenance Quality</td>
			<td><p>Các lỗi liên quan đến công tác bảo trì như vệ sinh, cân chỉnh, bôi trơn, thang chạy rung lắc, thiết bị hư hỏng nhưng không thay thế (cáp mòn, cáp tưa, thiết bị biến dạng,...), giãn cáp, thiết bị sét rỉ nặng, không thực hiện các công việc trong checklist bảo trì</p></td>
			<td>Scale x Weight</td>
			<td>Variable</td>
			<td>Variable</td>
		</tr>
		<tr>
			<td class="english-term">Behaviour & Attitude</td>
			<td><p>Giao tiếp với khách hàng, đồ nghề, dụng cụ PPE, nhận thức về an toàn, chất lượng,...<p></td>
			<td>Scale x Weight</td>
			<td>Variable</td>
			<td>Variable</td>
		</tr>
		<tr>
			<td class="english-term">Corrective Action</td>
			<td><p>Các hạng mục cần được khắc phục do lỗi sản phẩm theo yêu cầu nhà máy. Khi FI/DLFI được báo là hoàn thành, Inspector kiểm tra FI/DLFI được thực hiện đúng như yêu cầu hay không</p></td>
			<td>Scale x Weight</td>
			<td>Variable</td>
			<td>3</td>
		</tr>
		<tr>
			<td colspan="5" class="how-highlight"><p>Các hạng mục được ghi nhận là Immediate Risk NC thì Inspector phải thông báo cho SUP và SM, sau đó đánh giá mức độ rủi ro để đưa ra quyết định về hoạt động của thang, đảm bảo an toàn cho người sử dụng</p></td>
		</tr>
		<tr>
			<td colspan="5" class="how-highlight"><p>(*) Hệ thống sẽ tự động tính điểm, Inspector chỉ phải đánh dấu mức điểm theo thang nhập liệu. Thang đánh giá MQI cho fitter như trên</p></td>
		</tr>
	</table>
</div>

