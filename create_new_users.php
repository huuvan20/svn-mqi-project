<?php
if ( isset($_POST["new_user_role"]) && isset($_POST["new_username"]) && isset($_POST["new_password"]) && isset($_POST["new_user_email"]) && isset($_POST["new_fullname"]) ) {
	$new_user_role = $_POST["new_user_role"];
	$new_username = $_POST["new_username"];
	$new_password = $_POST["new_password"];
	$new_password_hash5 = md5($new_password);
	$new_user_email = $_POST["new_user_email"];
	$new_fullname = $_POST["new_fullname"];
	if ( !empty($new_user_role) && !empty($new_username) && !empty($new_password) && !empty($new_user_email) && !empty($new_fullname)) {
		require("inc/database.php");
		
		if ( preg_match('/\s/',$new_username) ) {
			$message_new = "Username không được chứa bất kì khoảng cách nào!";
			$flag = 1;
		} else {
			try {
				$results = $db->prepare("
					SELECT 	`id`
			    	FROM 	users 
			    	WHERE 	username =? 
				");
				$results->bindParam(1,$new_username);
				$results->execute();
			} catch (Exception $e) {
				echo "Không thể kết nối được với database: create_new_users 1. Xin hãy thử lại";
				print_r( $db->errorinfo());
				exit;
			}

			$row = $results->fetchColumn(0);

			if ( !empty($row) ) {
				$message_new = "Username này đã được tạo rồi, xin vui lòng chọn Username khác";
				$flag = 1;
			} else {

				try {
					$results = $db->prepare("
						INSERT INTO `users` (`user_role`,`username`,`password`,`user_email`,`fullname`)
						VALUES 				( ?, ?, ?, ?, ?)
					");
					$results->bindParam(1,$new_user_role);
					$results->bindParam(2,$new_username);
					$results->bindParam(3,$new_password_hash5);
					$results->bindParam(4,$new_user_email);
					$results->bindParam(5,$new_fullname);
					$results->execute();
				} catch (Exception $e) {
					echo "Không thể kết nối được với database: create_new_users 2. Xin hãy thử lại";
					print_r( $db->errorinfo());
					exit;
				}

				$message_new = "Chúc mừng bạn đã tạo thành công user mới!";
				$flag = 2;
			}
		}
	}
}


?>


<form method="POST" action="<?php echo $current_file; ?>">
	<table class="new-user-table">

		<tr class="new-user-title">
			<td colspan='2'>Tạo user mới</td>
		</tr>

		<tr>
			<td>Username</td>
			<td><input type="text" name="new_username" class="new-user-fill" maxlength="50" required /></td>
		</tr>

		<tr>
			<td>Password</td>
			<td><input type="password" name="new_password" class="new-user-fill" maxlength="50" required /></td>
		</tr>

		<tr>
			<td>Vai trò</td>
			<td><select name="new_user_role" class="new-user-fill" required>
				<?php
					foreach ( $user_role_list as $user_role ) { ?>
					<option value="<?php echo $user_role; ?>"><?php echo $user_role; ?></option>
				<?php	} 
				?>
			</select></td>
		</tr>

		<tr>
			<td>Họ và tên</td>
			<td><input type="text" name="new_fullname" class="new-user-fill" maxlength="50" required /></td>
		</tr>

		<tr>
			<td>Email</td>
			<td><input type="text" name="new_user_email" class="new-user-fill" maxlength="50" required /></td>
		</tr>

		<tr>
			<td colspan='2'>
				<input type="submit" name="submit" value="Tạo mới" class="button"/>
			</td>
		</tr>

		<?php if ( isset($message_new) && !empty($message_new) ) { ?>
		<tr 
		<?php if ( $flag == 1 ) {
				echo 'class="new-user-alert1"';
			} elseif ( $flag == 2 ) {
				echo 'class="new-user-alert2"';
			}
		?> >
			<td colspan='2'><?php echo $message_new;?></td>
		</tr>
		<?php } ?>

	</table>
</form>
